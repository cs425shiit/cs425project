<html>
<head><title>Class Roster</title></head>
<body>
<?php include_once './inc/nav.html';?>
<?php
	include './inc/connect.inc';
	include './inc/IDinfo.php';
	include './inc/StudentInfo.php';
	include './inc/CourseInfo.php';
	include './inc/CommunityInfo.php';

	$cid = $_GET['cid'];
// you must be logged in
// you must be the professor of this course
	$c = new CourseInfo($conn);
	$course = $c->getCoursefromID($cid);
    $user = $c->getID($_COOKIE['test']);
if(!$user) {
	echo "<div class='error'>Sorry! You must be logged in to do that!</div>";
	include './inc/loginscript.php';
}
else {
	echo "<div class='title'><h2>Course Roster for " . $course['department'] . $course['course_number'] . " - " . $course['name'] . "</h2></div><div class=\"container\">";
	if ($user['id'] <> $course['teacher']) {
		echo "<div class='error'>Sorry, this information is only available to the teacher of this course. </div>";
	}
	else {
		if (isset($_POST['submit'])) {
			$sum = 0;
			for ($x = $_POST['count']; $x >= 0; $x--) {
				$student = $_POST[$x];
				$grade = $_POST[$student];
				$c->updateStudentGrade($cid, $student, $grade);		
				$sum += $grade;		
			}
			$avg = $sum / $_POST['count'];
			$c->updateCourseGPA($avg, (int)$cid);
			echo "Grades submitted.";
		}
		$count = 0;
		$students = $c->getStudentsInCourse($cid);
		if($students) {
			echo "<div class='content'><div class='contentleft'><form method=\"post\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $cid .  "\"><table><tr><th>Student Name</th><th>Grade</th></tr>\n";
			foreach ($students as $row) {
			echo "<tr><td>" . $row['first_name'] . " " . $row['last_name'] . "\n</td><td><input type=\"number\" name=\"" . $row['id'] . "\" value=\"" . $row['grade'] . "\" min=\"0\" max=\"4\" size=\"3\" step=\".01\"><input type=\"hidden\" name=\"" . $count++ . "\" value=\"" . $row['id'] . "\"></td></tr>";
			}
			echo "</table><input type=\"hidden\" name=\"count\" value=\"" . $count . "\"><input type=\"submit\" value=\"submit\" name=\"submit\"></form>";
		}
	else {
		echo "No students are currently registered for this course.";
		}
	
	echo "</div><div class='contentright'>";

	$TAs = $c->getTAs($cid);
			if (empty($TAs)) {
				echo "<table id='TAs'>No TAs are assigned.</table>";
			}
			else {
				echo "<table id='TAs'><tr><th>TAs</th></tr>";
				foreach ($TAs as $row) {
					echo "<tr><td><a href=\"profile.php?uid=" . $row['id'] . "\">" . $row['first_name'] . " " . $row['last_name'] .
						 "</a></td></tr>";
				}
				echo "</table><br>";
			}
	if(isset($_POST['tasubmit'])) {
			$err = '';
			$success = $c->assignTA($_POST, $err);
			if ($success) {
				echo "<div class='inlineerr'>TA added. Moderators must be approved by the site admin. Once approved, please provide TA with their username and password.</div>";
			}
			else {
				if ($err) {
					echo $err;
				}
				else {
					echo "<div class='inlineerr'>TA assign failed.<br>TA may already be assigned<br> or the username may be taken.</div>";
				}
			}
		}
		
		$s = new StudentInfo($conn);
		$allstudents = $s->getAllStudents();
		echo "<div id='addta'><form method=\"post\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $cid .  "\">" .
			"<br>Add a TA:<br><select name='uid' align='right' size = '4'>";
			foreach($allstudents as $row) {
				echo "<option value=\"" . $row['id'] . "\">" .
					 $row['first_name'] . " " . $row['last_name'] .
					"</option>";
			}
		echo "</select><input type=\"hidden\" name=\"cid\" value=\"" . $cid . 				 "\"><br><input type='text' name='username' placeholder='TA moderator name'><br><input type='password' name='pass' placeholder='password'><br><input type=\"submit\" value=\"submit\" name=\"tasubmit\"></form></div>"; 
	
	}
}		
?>



</div>
</div>
</div>

</body>
</html>
