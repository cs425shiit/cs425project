<html>
<head><title>Results</title></head>
<body>
<?php
include './inc/connect.inc';

// Check connection
if ($conn->errorCode() == false) {
    die("Connection failed: " . $conn->errorCode());
}

try {
    $sql = $_POST['query'];
    $result = $conn->query($sql);

    $result->setFetchMode(PDO::FETCH_ASSOC);

    if (stripos($_POST['query'], 'select') === 0) {
	$sql2 = "CREATE TEMPORARY TABLE IF NOT EXISTS tempTable AS " . $_POST['query'];
    $conn->exec($sql2);

    $sql3 = "SHOW COLUMNS FROM tempTable";
    $result3 = $conn->query($sql3);
    $result3->setFetchMode(PDO::FETCH_ASSOC);
	}
} catch (PDOException $e){
    echo "Error running SQL Query: " . $e;
}

if ($result) {
echo $result->rowCount();
if ($result->rowCount() > 1) {
    echo "<table border='1'>";
    // output data of each row
    while(($row = $result->fetch()) !== false) {

        echo "<tr>";

        while(($cols = $result3->fetchColumn()) !== false){
            echo "<td>".$cols."</td>";
        }

        echo "</tr>";
        echo "<tr>";

        foreach($row as $value)
        {
            echo "<td>".$value."</td>";
        }

        echo "</tr>";

    }
    echo "</table>";
} else {
    echo "0 results";
}
}

?>
<form action="adminPage.php">
    <fieldset>
        <input type="submit" value="Return to Admin Page" />
    </fieldset>
</form>
</body>
</html>
