<html>
<head><title>Home</title></head>
<body>
<?php include_once './inc/nav.html';?>

<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
  include './inc/IDinfo.php';
  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';
  include './inc/CourseInfo.php';
  include './inc/CommunityInfo.php';
  include './inc/MessageBoard.php';
$username = $_COOKIE["test"];
$i = new IDinfo($conn);
if(!isset($username)) {
	echo "<div class='title'><h2>Home</h2></div><div class=\"container\">";
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";		
}
else {
	$person = $i->getID($username);
	echo "<div class='title'><h2>Welcome, " . ucwords($person['first_name']) . " " . ucwords($person['last_name']) . "!</h2></div>";

	echo "<div class=\"container\"><div class='content'><div class='contentleft'>";
	echo "<ul><li>" . $i->Linkify($person['id'], "See your public profile");
?>
	</li><li><a href="./update.php">Update your profile/settings</a></li><li>
	<a href="./addexperiences.php">Add job & experience info</a></li></ul>

<?php
	$s = new StudentInfo($conn);
	$c = new CourseInfo($conn);
	$student = $s->getStudent($username);
	if(!$student) {
		$f = new FacultyInfo($conn);
		$faculty = $f->getFaculty($username);
		if($faculty) {
			echo "<h3><i>FACULTY</i></h3>";
			echo "<h3>Your Courses</h3>";
		$courses = $c->getCoursesByFaculty($username);
			echo "<table><th>Course Name</th><th>Semester</th><th>Class GPA</th><th>Roster</th>";
			foreach ($courses as $row) {
				if($row['approved'] == 1) {
					echo "<tr><td><a href=\"./community.php?cid=" . $row['id'] . "\">" . 
						  $row['department'] . $row['course_number'] . " - " . $row['name'] . "</a></td>";
					echo "<td>" . ucfirst($row['semester']) . " " . $row['year'] . "</td><td>" . $row['GPA'] . 							  "</td><td><a href=\"./roster.php?cid=" . $row['id'] . "\">go</a></td></tr>";
				}
			}
			echo "</table><h6><a href=\"./facultyManageCourses.php\">add more courses</a></h6>";
		}
 	}
	else {
		echo "<h3>Your Courses</h3>";
		$courses = $c->getUserCourses($username);
			echo "<table>";
			foreach ($courses as $row) {
				if($row['approved'] == 1) {
					echo "<tr><td><a href=\"./community.php?cid=" . $row['id'] . "\">" . 
						  $row['department'] . $row['course_number'] . " - " . $row['name'] . "</a></td>";
					echo "<td>" . ucfirst($row['semester']) . " " . $row['year'] . "</td><td>" . $row['grade'] . 							  "</td></tr>";
				}
			}
			echo "</table><h6><a href=\"./communities.php\">find more courses</a></h6>";

		
	}
	echo "<h3>Your Clubs</h3>";
		$g = new CommunityInfo($conn);
		$clubs = $g->getUserClubs($username);
		echo "<table>";
		foreach ($clubs as $row) {
				if($row['approved'] == 1) {
					echo "<tr><td><a href=\"./community.php?cid=" . $row['cid'] . "\">" . $row['name'] . "</a></td>"
							. "<td><i>username: " . $row['username'] . "</td></tr>";
				}
			}
			echo "</table><h6><a href=\"./communities.php\">find more clubs</a></h6>";

		echo "<h3>Your Groups</h3>";
		$groups = $g->getUserGroups($username);
		echo "<table>";
		foreach ($groups as $row) {
				if($row['approved'] == 1) {
					echo "<tr><td><a href=\"./community.php?cid=" . $row['cid'] . "\">" . 
						  $row['name'] . "</a></td>" . "<td><i>username: " . $row['username'] . "</td></tr>";
				}
			}
			echo "</table><h6><a href=\"./communities.php\">find more groups</a></h6>";

		echo "</div><div class='contentright'><div class='discussions'><h3>Latest Discussions</h3>";
$mb = new MessageBoard($conn);
			$recent = $mb->user5($person['id']);
			echo "<table id=\"user5\"><tr><th colspan = 3>Community / Board</th></tr>";
			foreach ($recent as $row) {
				$d = new DateTime($row['time_posted']);
				$date = $d->format('M jS, Y<\b\\r>g:i a');
				 echo "<tr><td colspan='3'><a href=\"./community.php?cid=" . $row['cid'] . "\">" . $row['name'] . "</a></td></tr><tr><td colspan='3'>" . $mb->Linkify($row['cid'], $row['did'],$row['title']) . "</td></tr><tr><th>Comment</th><th>Posted By</th><th>Time</th></tr>" .
					  "<tr><td>" . $row['message'] . "</td>" .
					  "<td>" . $row['username'] . "</td>" .
					  "<td>" . $date . "</td>" .
					  "</tr><tr><td colspan='3'><hr></td></tr>";
				}
			echo  "</table>";
}
/*
	
	
	}
}
else {
	
<?php 
} }*/
?>
</div>
</div>
</div>
</div>
</body>
</html>
