<html>
<head><title>Community Moderators</title></head>
<body>
<?php include_once './inc/nav.html'; ?>
<div class='title'><h2>Community Moderators</h2></div><div class="container">

<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
include './inc/connect.inc';
include './inc/CommunityInfo.php';
include './inc/CourseInfo.php';
include './inc/Search.php';
$username = $_COOKIE["test"];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";		
}
else { 
	$c = new CommunityInfo($conn);
	$mods = $c->getAllMods();
	echo "<table id='allmods'><tr><th>Name</th><th>Community</th></tr>";
	foreach ($mods as $row) {
		echo "<tr><td><a href='./profile.php?uid=" . $row['id'] . "'>" . 
			$row['first_name'] . " " . $row['last_name'] . "</a></td>" .
			"<td><a href='./community.php?cid=" . $row['cid'] . "'>" .
			$row['name'] . "</a></td></tr>";
	}
	echo "</table>";
	
}
?>
</p></div>
</body>
</html>
