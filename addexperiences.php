<html>
<head>
<title>Experiences</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<h2><div class='title'>Add Internships & Experience</h2></div><div class='container'>
<?php 
// TO DO - BETTER UX FOR DATE ENTRY
  include './inc/connect.inc';
  include './inc/ExperienceInfo.php';
  $username = $_COOKIE["test"];
  $e = new ExperienceInfo($conn);
if (!isset($username)){
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else {
if(isset($_POST['submitjob']) && !isset($_POST['addmore'])) {
		if($_POST['end_date'] == '')
			$_POST['end_date'] = '0000-00-00';
		$job = $e->insertExperience($_POST, $username);
		echo "Your experience has been recorded."; ?>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<input type="submit" name ="addmore" value ="add more">
		<?php
		include "./inc/jobstable.inc";
} 
	else {?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <input type="text" placeholder="Company" name="company" "maxlength="20" required />
  	<select name="type">
		<option value="full-time">Full-Time</option>
		<option value="part-time">Part-Time</option>
		<option value="internship">Internship</option>
		<option value="co-op">Co-op</option>
		<option value="research">Research</option>
		<option value="project">Project</option>
	</select><br>
  <input type="text" placeholder="Position" name="position" maxlength="30"/>
  <input type="text" placeholder="Description" name="description" maxlength="100" />
  <input type="date" placeholder="Start Date (YYYY-MM-DD)" name="start_date" required/>
  <input type="date" placeholder="End Date" name="end_date" />
  <input type="submit" name="submitjob" value="add"/>
</form>
<?php include "./inc/jobstable.inc";
} 
}
?>
</div>
</body>
</html>

