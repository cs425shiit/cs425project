<html>
<head><title>Join Community</title></head>
<body>
<?php include_once './inc/nav.html';?>
<?php
// if user isn't logged in -> you gotta login, bro
// get the type of the community ID
// display relevant information
// get user to pick username
// get user to pick password
// and reenter password
// and click join!

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
//  include './inc/SettingsInfo.php';
//  include './inc/StudentInfo.php';
//  include './inc/FacultyInfo.php';
  include './inc/CommunityInfo.php';
  include './inc/CourseInfo.php';
  include './inc/MessageBoard.php';
  include_once './inc/IDinfo.php';
$username = $_COOKIE["test"];
$i = new IDinfo($conn);
$visitor = $i->getID($username);
$cid = $_GET['cid'];
$c = new CommunityInfo($conn);
$comm = $c->getCommunityfromID($cid);
$type = $comm['type'];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else {
		if (!$comm && !isset($_POST['join'])) {
			echo "<div class='error'>hmmm... something went wrong. We don't know that group. Try searching for them.</div>";
		}
		else {
			echo "<div class='title'><h2>Register to join " . $c->Linkify($cid, $comm['name']) . "</h2></div><div class='container'>"; 
		if (isset($_POST['join']) && ($_POST['pass'] <> $_POST['password2']))
		{
			echo "<div class='error'>Yikes, passwords don't match!</div>";
		}
		else if (isset($_POST['join']) && ($_POST['pass'] == $_POST['password2'])) 
		{
			$err_string = "";
			$success = $c->insertUser($username, $_POST, $err_string);
			if (!$success) {
				echo "<div class='error'>" . $err_string;
				echo " <a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $_POST['cid'] . "\">Go Back!</a></div>";
				}
			if ($success) {
				echo "Your registration has been submitted. You will be able to post to the message boards after approval from the moderators." .
				"<br><div class='error'>Demo: you can go approve yourself <a href=\"./demoApprove.php?cid=";
				echo $_POST['cid'];
				echo "\">here</a> on the admin page.</div>" ;
			}
		}

		else { ?>
			Please choose a username and password for posting to the message boards of this community.
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
			<fieldset>
			<legend>register!</legend>
			desired username:<br>
			<input type="text" name="username" required><br>
			password:<br>
			<input type="password" name="pass" required><br>
			password (again):<br>
			<input type="password" name="password2" required>
			<input type="hidden" name="cid" value="<?php echo $cid; ?>">
		<br><br>
			<input type="submit" value="register" name="join">
			</fieldset>
			</form>

<?php
		echo "<br><div class='error'>Demo: approve users <a href=\"./demoApprove.php?cid=";
				echo $cid;
				echo "\">here</a> on the admin page.</div>";
	}
	}



}

?>
</div>
</body>
</html>
