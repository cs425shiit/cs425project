<html>
<head>
<title>Delete Course</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>FACULTY PAGE: Add/Delete Courses</h2></div><div class="container">
<?php 
  include './inc/connect.inc';
  include './inc/CourseInfo.php';
  include './inc/FacultyInfo.php';;
  $username = $_COOKIE["test"];
  $c = new CourseInfo($conn);
  $f = new FacultyInfo($conn);
  $courseid = $_GET["cid"];
  $course = $c->getCourseFromID($courseid);
  $fac = $f->getFaculty($username);
if(!isset($username)){
	echo "<p>You are not logged in.</p>";
		echo "<p>login: ";
		include "./inc/loginscript.php";
}
else if (!$fac) {
		echo "<p>You do not have permission to delete this course. You may have been logged out.</p>";
		echo "<p>login as faculty: ";
		include "./inc/loginscript.php";
		echo "<br> or  <a href =\"./facultyManageCourses.php\">Return to your Courses</a></p>";
}
else {
if(isset($_POST['yes'])) {
	$userid = $f->getFaculty($username);
// this is not a very secure way of doing this but it does prevent people
// accidentally deleting other users' information.

	if( $_POST['courseuser'] == $userid['id'] ) {
		$success = $c->deleteCourse($_POST['cid']);
		if($success) {
			echo "Course has been deleted! <a href =\"./facultyManageCourses.php\">Show your Courses</a>";
		}
		else {
			echo "Something went wrong. Course could not be deleted. <a href =\"./facultyManageCourses.php\">Return to Courses</a>";
		}
	}
	else {
		echo "<p>You do not have permission to delete this course. You may have been logged out.</p>";
		echo "<p>login: ";
		include "./inc/loginscript.php";
		echo "<br> or  <a href =\"./facultyManageCourses.php\">Return to Courses</a></p>";
	}
}
else {
?>
  Are you sure you want to delete this course?<br>
<?php
		echo "<table style='text-align:center'><tr>\n";
		// add the table headers
		echo "<th>Dept</th><th>Course No.</th><th>Semester</th><th>Year</th><th>Avg GPA</th></tr></tr><tr><td colspan =\"6\"><hr></td></tr>";// display data
    	print "<tr><td>";
		echo $course['name'] . "</td><td>";
    	echo $course['department'] . "</td><td>";
		echo $course['course_number'] . "</td><td>";
		echo $course['semester'] . "</td><td>";
		echo $course['year'] . "</td><td>";
		echo $course['GPA'] . "</td>";
        echo "</tr><tr><td colspan =\"6\"><hr></td><tr></table>\n";
		echo "<form method=\"POST\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\"><input type = \"hidden\" name =\"courseuser\" value=\"" . $course['teacher'] . "\">";
		echo "<input type=\"hidden\" name =\"cid\" value = \"" . $courseid . "\"><input type=\"submit\" value=\"yes\" name=\"yes\"><a href =\"./facultyManageCourses.php\">No, take me back!</a></form>";
}}
?>

 
</body>
</html>

