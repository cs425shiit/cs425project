<html>
<head>
<title>Experiences</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<h2>Add Internships & Experience</h2>
<?php 
// TO DO - finish YES -> delete, NO -> go back
  include './inc/connect.inc';
  include './inc/ExperienceInfo.php';
  include './inc/IDinfo.php';
  $username = $_COOKIE["test"];
  $e = new ExperienceInfo($conn);
  $i = new IDinfo($conn);
  $jobid = $_GET["jid"];
  $job = $e->getExperienceFromID($jobid);

if(isset($_POST['yes'])) {
	$userid = $i->getID($username);
// a little confusing name here. $job['id'] refers to a person assoc with the job.
// also this is not a very secure way of doing this but it does prevent people
// accidentally deleting other users' information.
	if( $_POST['jobuser'] == $userid['id'] ) {
		$success = $e->deleteExperience($_POST['jid']);
		if($success) {
			echo "Job has been deleted! <a href =\"./addexperiences.php\">Return to Jobs & Experiences</a>";
		}
		else {
			echo "Something went wrong. Job could not be deleted. <a href =\"./facultyManageCourses.php\">Return to Jobs & Experiences</a>";
		}
	}
	else {
		echo "<p>You do not have permission to delete this course. You may have been logged out.</p>";
		echo "<p>login: ";
		include "./inc/loginscript.php";
		echo "<br> or  <a href =\"./facultyManageCourses.php\">Return to your Courses</a></p>";
	}
}
else {
?>
  Are you sure you want to delete this job?<br>
<?php
	echo "<table width=\"100%\" border=\"0|0\"><tr>\n";
	// add the table headers
	echo "</tr>";// display data
    	print "<tr><td>";
    	echo $job['company'] . "</td><td><i>";
		echo $job['type'] . "</i></td><td>";
		$date = new DateTime($job['start_date']);
		echo date_format($date, "m/j/Y") . "</td>";
		if ($job['end_date'] != "0000-00-00") {
			$edate = new DateTime($job['end_date']);
			echo "<td>" . date_format($edate, "m/j/Y") . "</td>";
		}
		else {
			echo "<td>present</td>";
		}
		echo "</tr><tr><td>";
		echo $job['position'] . "</td><td colspan =\"3\">";
		echo $job['description'] . "</td>";
        echo "</tr><tr><td colspan =\"4\"><hr></td>\n";
		echo "</table>\n";
		echo "<form method=\"POST\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\"><input type = \"hidden\" name =\"jobuser\" value=\"" . $job['userid'] . "\">";
		echo "<input type=\"hidden\" name =\"jid\" value = \"" . $jobid . "\"><input type=\"submit\" value=\"yes\" name=\"yes\"><a href =\"./addexperiences.php\">No, take me back!</a></form>";
}
?>

 
</body>
</html>

