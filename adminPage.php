<html>
<head>
    <title>Admin Page</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<div class="title"><h2>Admin Page</h2></div><div class="container">
<!-- I'm assuming that this is the dashboard for admin so I've added these for ease of access. Feel free to move them. -->
<div id ='admin'><?php include './approveCourse.php'; ?></div> 
<div id ='admin'><?php include './approvegroups.php'; ?></div>

<form action="displayTable.php" method="post">
    <fieldset>
        <input type="radio" value="Student" name="table">Student</br>
        <input type="radio" value="Faculty" name="table">Faculty</br>
        <input type="radio" value="Community" name="table">Community</br>
        <input type="radio" value="Course" name="table">Course</br>
        <input type="radio" value="Discussion" name="table">Discussion</br>
        <input type="radio" value="Experience" name="table">Experience</br>
        <input type="radio" value="Forum_comment" name="table">Forum Comment</br>
        <input type="radio" value="Community_User" name="table">Community User</br>
        <input type="radio" value="ID" name="table">ID</br>
        <input type="radio" value="TA" name="table">TA</br>
        <input type="radio" value="Moderator" name="table">Moderator</br>
        <input type="radio" value="Settings" name="table">Settings</br>
        <input type="Submit" />
    </fieldset>
</form>
<form action="execQuery.php" method="post">
    <fieldset>
        <input type="text" name="query" size="100">SQL Query<br>
        <input type="submit" value="Execute Query" />
    </fieldset>
</form>
<form action="massInsert.php" method="post">
    <input type="submit" value="Insert Test Data" />
</form>
<form action="clearAllTables.php" method="post" onsubmit="return confirm('This will delete ALL data. Are you sure you want to do this?')">
    <input type="submit" value="Delete ALL Data" />
</form>
</div>
</body>
</html>
