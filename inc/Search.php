<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
class Search{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

	function getNamesLike($string, &$count) {
		$sql = "SELECT * FROM ID WHERE (CONCAT(first_name, ' ', last_name)) LIKE ?";
		$wstring = "%" . $string . "%";
		$stm = $this->db->prepare($sql);
		$stm->execute([$wstring]);
		$count = $stm->rowCount();
		return $stm->fetchAll();
	}

	function getGroupsLike($string, &$count) {
		$sql = "SELECT * FROM Community WHERE name LIKE ?";
		$wstring = "%" . $string . "%";
		$stm = $this->db->prepare($sql);
		$stm->execute([$wstring]);
		$count = $stm->rowCount();
		return $stm->fetchAll();
	}

		
}

		
