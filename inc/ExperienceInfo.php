<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
// Experience schema:
// Experience (
//	id,
//	type,
//	position,
//	company,
//	description,
//  start_date,
//  end_date,
//	id );
class ExperienceInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// fetches ONE row by id (experience id)
	function getExperiencefromID($id)
	{
		$sql = "SELECT * FROM Experience WHERE id = :id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':id' => $id]);
		return $stm->fetch();
	}
// can be user id or username. fetches ALL rows
	function getExperiences($user)
	{		
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			$id = $stm->fetch();
			$user = $id['id'];
		}
		$sql = "SELECT * FROM Experience WHERE userid = :userid";
		$stm = $this->db->prepare($sql);
		$stm->execute([':userid' => $user]);
		return $stm->fetchAll();
	}

// $user can be username or ID.
	function insertExperience(array $e_arr, $user) 
	{
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			$id = $stm->fetch();
			$user = $id['id'];
		}
			$sql = "INSERT INTO Experience (userid, type, position, company, 
											description, start_date, end_date) 
									VALUES (:userid, :type, :position, :company, 
											:desc, :start, :end)";
		$stm = $this->db->prepare($sql);
  		return $stm->execute(array(':userid' => $user, ':type' => $e_arr['type'], ':position' => $e_arr['position'], ':company' => $e_arr['company'], ':desc' => $e_arr['description'], ':start' => $e_arr['start_date'], ':end' => $e_arr['end_date']));
		
	}

	function updateExperience(array $e_arr, $jobid) {
		$sql = "UPDATE Experience SET 
								type = :type, 
								position = :position,
								company = :company,
								description = :description,
								start_date = :start_date,
								end_date = :end_date,
								WHERE id = :ref";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':type' => $e_arr['type'], ':position' => $e_arr['position'], ':company' => $e_arr['company'], ':description' => $e_arr['description'], ':start_date' => $e_arr['start'], ':end_date' => $e_arr['end'], ':id' => $jobid));
		
	}

	function deleteExperience($experience_id) {
		$sql = "DELETE FROM Experience WHERE id = :exp_id";
		$stm = $this->db->prepare($sql);
		return $stm->execute([':exp_id' => $experience_id]);
	}

}

		
