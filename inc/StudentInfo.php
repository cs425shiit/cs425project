<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
class StudentInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// getStudent returns row indexed by column name
// return 0 if user is not student, if applicable
// takes username or id
	function getStudent($user)
	{
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			$id = $stm->fetch();
			$user = $id['id'];
		}
			$sql = "SELECT ID.*, GPA, start_semester, is_TA, degree_status, degree_type FROM ID, Student WHERE ID.id = Student.id AND Student.id = ?";
			$stm = $this->db->prepare($sql);
			$stm->execute(array($user));
			return $stm->fetch();
	}

	// warning! will return ev-er-y-body, no matter how many! should be changed once a significant # of users exist
	function getAllStudents() {
		$sql = "SELECT Student.id as id, username, first_name, last_name FROM Student, ID WHERE Student.id = ID.id ORDER BY last_name ASC";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	}

// insertStudent returns TRUE on success or FALSE on failure
// takes $_POST array of student details.
	function insertStudent(array $s_arr, $id) {
		$sql = "INSERT INTO Student (start_semester, GPA, id, degree_status, degree_type) VALUES (:startsem, :gpa, :id, :degstat, :degtype)";
		$stm = $this->db->prepare($sql);
  		return $stm->execute(array(':startsem' => $s_arr['start_semester'], ':gpa' => $s_arr['GPA'], ':id' => $id, ':degstat' => $s_arr['degree_status'], ':degtype' => $s_arr['degree_type']));
	}

	function updateStudentInfo(array $s_arr, $id) {
		$sql = "UPDATE Student, ID SET first_name = :first,
							last_name = :last,							
							start_semester = :startsem,
							GPA = :gpa,
							degree_status = :degstat,
							degree_type = :degtype
							WHERE Student.id = ID.id AND ID.id = :id";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':first' => $s_arr['first_name'], ':last' => $s_arr['last_name'], ':startsem' => $s_arr['start_semester'], ':gpa' => $s_arr['GPA'], ':degstat' => $s_arr['degree_status'], ':degtype' => $s_arr['degree_type'], ':id' => $id));

	}
function getTACourses($uid){
	$sql = "SELECT * FROM Course, Community, TA WHERE TA.id = :uid AND Community.type = 'course' AND TA.cid = Community.id AND Community.id = Course.id";
	$stm = $this->db->prepare($sql);
	$stm->execute([':uid' => $uid]);
	return $stm->fetchAll();
}
}
