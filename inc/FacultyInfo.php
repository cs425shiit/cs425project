<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
class FacultyInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// getFacultyreturns row indexed by column name
// return 0 if user is not student, if applicable
// takes username or id
	function getFaculty($user)
	{
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			$id = $stm->fetch();
			$user = $id['id'];
		}
			$sql = "SELECT first_name, last_name, start_year, Faculty.id as id, position FROM Faculty, ID WHERE Faculty.id = ID.id AND Faculty.id = ?";
			$stm = $this->db->prepare($sql);
			$stm->execute(array($user));
			return $stm->fetch();
	}

	// warning! will return ev-er-y-body, no matter how many! should be changed once a significant # of users exist
	function getAllFaculty() {
		$sql = "SELECT username, first_name, last_name FROM Faculty, ID WHERE Faculty.id = ID.id";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
	}
// insertFaculty returns TRUE on success or FALSE on failure
// takes $_POST array of faculty details.
	function insertFaculty(array $f_arr, $id) {
		$sql = "INSERT INTO Faculty (position, id) VALUES (:position, :id)";
		$stm = $this->db->prepare($sql);
  		return $stm->execute(array(':position' => $f_arr['position'], ':id' => $id));
	}

	function updateFacultyInfo(array $f_arr, $id) {
		$sql = "UPDATE Faculty, ID SET first_name = :first,
								   last_name = :last, 
								   position = :position 
								   WHERE Faculty.id = ID.id AND ID.id = :id";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':first' => $f_arr['first_name'], ':last' => $f_arr['last_name'], ':position' => $f_arr['position'], ':id' => $id));
		
	}

	function getFacultyHi($id) {
		$sql = "SELECT a.department, a.course_number, b.GPA
FROM Course a
  INNER JOIN (
               SELECT department, course_number, MAX(GPA) AS GPA FROM (
                 (SELECT * FROM Course WHERE TEACHER = ?) AS courses)
             ) b ON a.GPA = b.GPA";
		$stm = $this->db->prepare($sql);
		$stm->execute($id);
		return $stm->fetch();
	}	

}

		
