<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
// Course schema:
// Course (
//	department
//	course_numer
//	teacher
//	GPA
//	semester
//	year
//	id );

include_once './inc/IDinfo.php';
class CourseInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// get course info from course id (id) returns ONE row
	function getCoursefromID($id)
	{
		$sql = "SELECT * FROM Course, Community WHERE Course.id = Community.id AND Course.id = :id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':id' => $id]);
		return $stm->fetch();
	}
	
	function getCourses(){
	  $sql = "SELECT name, Community.id AS id, department, course_number, last_name, first_name, semester, year, GPA, teacher FROM Community, Course, ID WHERE Community.id = Course.id AND Community.approved = 1 AND ID.id = Course.teacher ORDER BY Course.year DESC, FIELD(Course.semester, 'summer', 'fall', 'spring'), Course.department ASC, Course.course_number ASC, Course.id DESC";
	  $stm = $this->db->prepare($sql);
	  $stm->execute();
	  return $stm->fetchAll();
	}

	// returns [name, department, coursenumber, semester, year, teacher, id, approved, grade]
	function getUserCourses($user) {
		if (!is_numeric($user)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($user);
			$user = $id['id'];
		}
			$sql = "SELECT name, Community.id AS id, department, course_number, semester, year, teacher, Community_User.approved as approved, Community_User.grade as grade FROM Community, Course, Community_User WHERE Community.id = Course.id AND Course.id = Community_User.cid AND Community.approved = 1 AND Community_User.userid = :id ORDER BY Course.year DESC, FIELD(Course.semester, 'summer', 'fall', 'spring'), Course.department ASC, Course.course_number ASC, Course.id DESC";
			$stm = $this->db->prepare($sql);
			$stm->execute([':id' => $user]);
			return $stm->fetchAll();
		}

// get all courses taught by $faculty. can be username or ID.
	function getCoursesByFaculty($faculty)
	{
		if (!is_numeric($faculty)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($faculty);
			$faculty = $id['id'];
		}
		$sql = "SELECT Community.name AS name, Community.id AS id, Community.approved AS approved, Course.department as department, Course.course_number as course_number, Course.teacher as teacher, Course.semester as semester, Course.year as year, Course.GPA as GPA FROM Community, Course WHERE Community.id = Course.id AND Course.teacher = :id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':id' => $faculty]);
		return $stm->fetchAll();
	}

	function getStudentsInCourse($cid) {
		$sql = "SELECT first_name, last_name, grade, approved, id FROM Community_User, ID WHERE Community_User.userid = ID.id AND Community_User.cid = :cid";
		$stm = $this->db->prepare($sql);
		$stm->execute([':cid' => $cid]);
		return $stm->fetchAll();
	}

	function updateStudentGrade($cid, $uid, $grade) {
		//try {
			$sql = "UPDATE Community_User SET grade = :grade WHERE Community_User.cid = :cid AND Community_User.userid = :uid";
			$stm = $this->db->prepare($sql);
			return $stm->execute([':grade' => $grade, ':uid' => $uid, ':cid' => $cid]);
		//}
		
	}
   function getTAs($community_id) {
	$sql = "SELECT ID.id as id, first_name, last_name FROM TA, ID WHERE TA.id = ID.id AND TA.cid = :community_id ORDER BY last_name ASC";
		$stm = $this->db->prepare($sql);
		$stm->execute([':community_id' => $community_id]);
		return $stm->fetchAll();
}

	function assignTA($arr, &$err) {
		try {
			$this->db->beginTransaction();
			$sql = "INSERT INTO TA (id, cid) VALUES (:uid, :cid)";
			$stm = $this->db->prepare($sql);
			$result1 = $stm->execute([':uid' => $arr['uid'], ':cid' => $arr['cid']]);
			if ($result1) {
			$sql = "INSERT INTO Moderator (uid, cid, username, pass, approved) VALUES (:uid, :cid, :user, :pass, 0)";
			$stm = $this->db->prepare($sql);
			$result = $stm->execute([':uid' => $arr['uid'], ':cid' => $arr['cid'], ':user' => $arr['username'], ':pass' => $arr['pass']]);
			}
			if ($result1 && $result) {
				$this->db->commit();
			}
			else {
				$this->db->rollBack();
			}
			
		}
		catch (\PDOException $e) {
				if ($e->errorInfo[1] == 1644) {
					$err =  "<div class='inlineerr'>TA assign failed.<br> Student enrolled in course.</div>";
    			}
		}
		return $result;		
	}
			
			
	function getUnapprovedCourses(){
	  $sql = "SELECT * FROM Course, Community WHERE Community.id = Course.id AND approved=0 AND Community.type='course'";
	  $stm = $this->db->prepare($sql);
	  $stm->execute();
	  return $stm->fetchAll();
	}

// $faculty can be username or ID.
	function insertCourse(array $e_arr, $faculty)
	{
		if (!is_numeric($faculty)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($faculty);
			$faculty = $id['id'];
		}
			$sql = "INSERT INTO Community (name, type)
									VALUES (:name, 'course')";
			$stm = $this->db->prepare($sql);
			$comm = $stm->execute(array(':name' => $e_arr['name']));
			$id = $this->db->lastInsertId();
			if ($comm) {
			$sql = "INSERT INTO Course (department, course_number, teacher, GPA, semester, year, id)
									VALUES (:department, :course_number, :teacher, :GPA, :semester, :year, :id)";
		$stm = $this->db->prepare($sql);
  		return $stm->execute(array(':department' => $e_arr['department'], ':course_number' => $e_arr['course_number'], ':teacher' => $faculty, ':GPA' => $e_arr['GPA'], ':semester' => $e_arr['semester'], ':year' => $e_arr['year'], ':id' => $id));
		}
		else return 0;
	}

	function updateCourseGPA($gpa, $courseid) {
		$sql = "UPDATE Course SET GPA = ? WHERE id = ?";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array($gpa, $courseid));

	}

	function deleteCourse($course_id) {
		$sql = "DELETE FROM Course WHERE id = :course_id";
		$stm = $this->db->prepare($sql);
		return $stm->execute([':course_id' => $course_id]);

	}
	function approveCourse($course_id){
		$sql = "UPDATE Community SET approved=1 WHERE id = :course_id";
		$stm = $this->db->prepare($sql);
		return $stm->execute([':course_id' => $course_id]);
	}


// from id because i'm lazy
function getID($user)
	{
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			return $stm->fetch();
		}
		else {
			$sql = "SELECT * FROM ID WHERE id = ?";
			$stm = $this->db->prepare($sql);
			$stm->execute([$user]);
			return $stm->fetch();
		}
	}
}
