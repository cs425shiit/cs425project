<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);

// methods:
// all get functions return 0 if no row selected (i.e. can be used as "exists?")
// getID($user) :: string -> arr[]
// getID($id) :: int -> arr[]
// getIDfromEmail($email) :: string -> arr[]
// insertID($user, $pass, $email) :: int -> string -> string -> bool
// updatePassword($user, $oldpass, $newpass) :: string -> string -> string -> bool

class IDinfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// getIDfrom_ functions return row indexed by column name
	function getID($user)
	{
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM ID WHERE username = :user";
			$stm = $this->db->prepare($sql);
			$stm->execute([':user' => $user]);
			return $stm->fetch();
		}
		else {
			$sql = "SELECT * FROM ID WHERE id = ?";
			$stm = $this->db->prepare($sql);
			$stm->execute([$user]);
			return $stm->fetch();
		}
	}

	function getIDfromEmail($email)
	{
		$sql = "SELECT * FROM ID WHERE email = :email";
		$stm = $this->db->prepare($sql);
		$stm->execute([':email' => $email]);
		return $stm->fetch();
	}

	function addlogin($user, $ip) {
		$uid = $this->getID($user)['id'];
		echo $uid;
		$sql = "INSERT INTO Logins (uid, dt, ip) VALUES (?, now(), ?)";
		$stmt = $this->db->prepare($sql);
		return $stmt->execute([$uid, $ip]);
		}

// insertID returns TRUE on success or FALSE on failure
	function insertID($arr, &$err) {
		try {
			$sql = "INSERT INTO ID (first_name, last_name, start_year, email, username, pass) VALUES(:first_name, :last_name, :start_year, :email, :username, :pass)";
			$stmt = $this->db->prepare($sql);
  			$stmt->execute(array(':first_name' => $arr['first_name'], ':last_name' => $arr['last_name'], ':start_year' => $arr['start_year'], ':email' => $arr['email'], ':username' => $arr['username'], ':pass' => $arr['pass']));
			$result = $this->getID($arr['username']);
		}
		catch (\PDOException $e) {
			if($e->errorInfo[1]) {
				$err = $e->getMessage();
				if ($e->errorInfo[1] == 1062) {
					$res = $this->getIDfromEmail($arr['email']);
					$err = "You are already registered! Your username is ". $res['username'];
					$result = 0;
				}
				//else {
				//$result = 0;
				//$err = "Unknown error";
				//}
			}
		}
		return $result;
	}

	function updateID($arr, $id) {
		$sql = "UPDATE Student SET first_name = :first,
					   last_name = :last,
					   start_year = :startyr
					WHERE id = :id";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':first' => $s_arr['first_name'], ':last' => $s_arr['last_name'], ':startyr' => $s_arr['start_year'], ':id' => $id));
	}

	function updatePassword($user, $oldpass, $newpass) {
		$sql = "SELECT * FROM ID WHERE username = ? AND pass = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute(array($user, $oldpass));
		$match = $stmt->fetch();
		if($match) {
			$sql = "UPDATE ID SET pass = ? WHERE username =	?";
			$stmt = $this->db->prepare($sql);
			return $stmt->execute(array($newpass,$user));
		}
		else {
			return NULL;
		}
	}
	function Linkify($uid, $text){
		$string = "<a href=\"profile.php?uid=$uid\">$text</a>";
		return $string;
	}
}
