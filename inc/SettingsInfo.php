<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
// Settings schema:
// Settings (
//	id,
//	show_start_semester_and_year
//	show_degree_status
//	show_degree_type
//	show_GPA
//	show_following );
include_once './inc/IDinfo.php';
class SettingsInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// getIDfrom_ functions return row indexed by column name
	function getSettings($user)
	{
		if (!is_numeric($user)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($user);
			$user = $id['id'];
		}
			$sql = "SELECT * FROM Settings WHERE id = ?";
			$stm = $this->db->prepare($sql);
			$stm->execute(array($user));
			return $stm->fetch();
		
	}


// It's assumed that a settings entry is created automatically for each Student registration.

	function updateSettings(array $s_arr, $id) {
		$sql = "UPDATE Settings SET 
								show_start_semester_and_year = :startbool, 
								show_degree_status = :degstatbool, 
								show_degree_type = :degtypebool, 
								show_GPA = :gpabool, 
								show_following = :followbool
								WHERE id = :id";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':startbool' => $s_arr['startbool'], ':degstatbool' => $s_arr['degstatbool'], ':degtypebool' => $s_arr['degtypebool'], ':gpabool' => $s_arr['gpabool'], ':followbool' => $s_arr['followbool'], ':id' => $id));
		
	}

}

		
