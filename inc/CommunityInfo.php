<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
// Community has method: getCommunityfromID($id)
// Club methods listed here, Group methods are same but replace Club with Group in name (duh)
// getUnapprovedClubs();
// insertClub($arr);
// deleteClub($id);
// approveClub($id);
// getUserClubs($user);

// ** METHODS THAT WORK WITH CLUB AND GROUP **
// approveUser($community_id, $user)
// approveAllUsers($community_id)
// getUnapprovedUsers($community_id)
// getUsers($community_id)

include_once './inc/IDinfo.php';
class CommunityInfo{

	function __construct($pdo)
	{
		$this->db = $pdo;
	}

// get community info from community id (id) returns ONE row
	function getCommunityfromID($id)
	{
		$sql = "SELECT * FROM Community WHERE id = :id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':id' => $id]);
		$comm = $stm->fetch();
		return $comm;
	}

	function getGroups(){
	  $sql = "SELECT * FROM Community WHERE type = 'interest' AND approved=1";
	  $stm = $this->db->prepare($sql);
	  $stm->execute();
	  return $stm->fetchAll();
	}
	function getClubs(){
	  $sql = "SELECT * FROM Community WHERE type = 'club' AND approved=1";
	  $stm = $this->db->prepare($sql);
	  $stm->execute();
	  return $stm->fetchAll();
	}
	function insertClub(array $e_arr)
	{
		$sql = "INSERT INTO Community (name, type)
									VALUES (:name, 'club')";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':name' => $e_arr['name']));
	}

	function insertGroup(array $e_arr)
	{
		$sql = "INSERT INTO Community (name, type)
									VALUES (:name, 'interest')";
		$stm = $this->db->prepare($sql);
		return $stm->execute(array(':name' => $e_arr['name']));

	}
	function deleteCommunity($cid) {
		$sql = "DELETE FROM Community WHERE id = :cid";
		$stm = $this->db->prepare($sql);
		return $stm->execute([':cid' => $cid]);

	}
	function approveCommunity($community_id){
		$sql = "UPDATE Community SET approved=1 WHERE id = :community_id";
		$stm = $this->db->prepare($sql);
		return $stm->execute([':community_id' => $community_id]);
	}

	function getUnapprovedCommunities(){
	  $sql = "SELECT * FROM Community WHERE approved=0 AND type<>'course'";
	  $stm = $this->db->prepare($sql);
	  $stm->execute();
	  return $stm->fetchAll();
	}





// $id can be ID.id or ID.user.. returns ALL of a user's interest groups
// returns [name, number, approved]
	function getUserGroups($user) {
		if (!is_numeric($user)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($user);
			$user = $id['id'];
		}
			$sql = "SELECT Community.name, Community.id as cid, Community.approved as capproved, Users.username as username, Users.uid as uid, Users.cid as cid, Users.approved as approved from ((SELECT username, userid as uid, cid, approved FROM Community_User)
UNION
(SELECT username, uid, cid, approved from Moderator)) as Users, Community WHERE type='interest' AND Users.cid = id AND Users.uid = :id";
			$stm = $this->db->prepare($sql);
			$stm->execute([':id' => $user]);
			return $stm->fetchAll();
	}

// returns [name, number, approved]
	function getUserClubs($user) {
		if (!is_numeric($user)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($user);
			$user = $id['id'];
		}
			$sql = "SELECT Community.name, Community.id as cid, Community.approved as capproved, Users.username as username, Users.uid as uid, Users.cid as cid, Users.approved as approved from ((SELECT username, userid as uid, cid, approved FROM Community_User)
UNION
(SELECT username, uid, cid, approved from Moderator)) as Users, Community WHERE type='club' AND Users.cid = Community.id AND Users.uid = :id";
			$stm = $this->db->prepare($sql);
			$stm->execute([':id' => $user]);
			return $stm->fetchAll();
	}

// MODERATOR INFO AND ACTIONS

   function getTAs($community_id) {
	$sql = "SELECT ID.id as id, first_name, last_name FROM TA, ID WHERE TA.id = ID.id AND TA.cid = :community_id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':community_id' => $community_id]);
		return $stm->fetchAll();
}
   function getMods($community_id) {
		$sql = "SELECT ID.id as id, first_name, last_name, Moderator.username as username, Moderator.approved as approved FROM ID, Moderator WHERE ID.id = Moderator.uid AND Moderator.cid = :community_id";
		$stm = $this->db->prepare($sql);
		$stm->execute([':community_id' => $community_id]);
		return $stm->fetchAll();
}

	function getAllMods() {
		$sql = "SELECT ID.id as id, first_name, last_name, Moderator.cid as cid, Moderator.username as username, AllCommunity.name as name FROM ID, Moderator, (SELECT * FROM
((SELECT CONCAT_WS(' ', department, course_number, name) AS name, Community.id FROM Course, Community WHERE Course.id = Community.id AND Community.approved = 1)
 UNION
 (SELECT name, Community.id FROM Community WHERE approved = 1 AND type <> 'course')) AS something) AS AllCommunity  WHERE ID.id = Moderator.uid AND AllCommunity.id = Moderator.cid AND Moderator.approved = 1 ORDER BY NAME ASC";
		$stm = $this->db->prepare($sql);
		$stm->execute();
		return $stm->fetchAll();
}
// expects user ID or comm username.
   function isMod($uid, $cid) {
		if (!is_numeric($user)) {
			$sql = "SELECT * FROM Moderator WHERE username = :uid AND cid = :cid";
			}
		else {
			$sql = "SELECT * FROM Moderator WHERE uid = :uid AND cid = :cid";
			}
		$stm = $this->db->prepare($sql);
		$stm->execute([':uid' => $uid, ':cid' => $cid]);
		$result = $stm->fetch();
		if($result)
			return 1;
		else
			return 0;
	}
// expects user ID or site username, but not comm username.
	function getUser($uid, $cid) {
		if (!is_numeric($user)) {
			$i = new IDinfo($this->db);
			$id = $i->getID($uid);
			$uid = $id['id'];
		}
		$sql = "SELECT Users.username as username, Users.uid as uid, Users.cid as cid from ((SELECT username, userid as uid, cid FROM Community_User WHERE approved = 1)
	UNION
	(SELECT username, uid, cid from Moderator WHERE approved = 1)) as Users, Community WHERE Users.cid = Community.id AND Community.id = :cid AND Users.uid = :uid";
		$stm = $this->db->prepare($sql);
		$stm->execute([':uid' => $uid, ':cid' => $cid]);
		return $stm->fetch();
	}

// returns first_name, last_name, Club/Group.username, ID.username, ID.id AS (first_name, last_name, nickname, username) for APPROVED users

	function getAllUsers($community_id) {
		$sql = "SELECT Community.name, Community.id as cid, Community.approved as capproved, Users.username as username, Users.uid as uid, Users.cid as cid from ((SELECT username, userid as uid, cid FROM Community_User WHERE approved = 1)
UNION
(SELECT username, uid, cid from Moderator WHERE approved = 1)) as Users, Community WHERE Users.cid = Community.id AND Community.id = :id";
			$stm = $this->db->prepare($sql);
			$stm->execute([':id' => $community_id]);
			return $stm->fetchAll();
		}

// same as above but gets unapproved users
		function getUnapprovedUsers($community_id) {
		 	$sql = "SELECT ID.username as username, ID.id, first_name, last_name, Community_User.username AS nickname, Community_User.cid FROM ID, Community_User WHERE ID.id = Community_User.userid AND Community_User.cid = :community_id AND Community_User.approved = 0";
			$stm = $this->db->prepare($sql);
			$stm->execute([':community_id' => $community_id]);
			return $stm->fetchAll();
	}

// NOTE! $user can be ID.id/Group_user.userid or Club/Group_User.username (but not ID.username)
		function approveUser($community_id, $user) {
			if(!is_numeric($user)) {
				$sql = "SELECT * FROM Community_User WHERE username = :user";
				$stm = $this->db->prepare($sql);
				$stm->execute([':user' => $user]);
				$id = $stm->fetch();
				$user = $id['userid'];
			}
				$sql = "UPDATE Community_User SET approved=1 WHERE userid = :userid AND cid = :cid";
				$stm = $this->db->prepare($sql);
				return $stm->execute(array(':userid' => $user, ':cid' => $community_id));
		}

		function approveAllUsers($community_id) {
			$users = $this->getUnapprovedUsers($community_id);
			foreach ($users as $row) {
				if ($row['id']) {
					$this->approveUser($community_id, $row['id']);
				}
			}
			return;
		}

	    function deleteUser($community_id, $user){
			$sql = "DELETE FROM Community_User WHERE userid = :user AND cid = :community_id";
			$stm = $this->db->prepare($sql);
			return $stm->execute(array(':user' => $user, 'community_id' => $community_id));
		}

// returns any type of user, including mod, expects uid or Community username, NOT siteusername.
// to use site username or uid use getUser,
		function isUser($uid,$cid) {
			if (!is_numeric($uid)) {
				$sql = "SELECT Users.username as username, Users.uid as uid, Users.cid as cid from ((SELECT username, userid as uid, cid FROM Community_User WHERE approved = 1)
UNION
(SELECT username, uid, cid from Moderator WHERE approved = 1)) as Users, Community WHERE Users.cid = Community.id AND Community.id = :cid AND Users.username LIKE :uid";
				$stm = $this->db->prepare($sql);
				$stm->execute(array(':cid' => $cid, ':uid' => $uid));
				return $stm->fetch();
			}
			else {
				$sql = "SELECT Users.username as username, Users.uid as uid, Users.cid as cid from ((SELECT username, userid as uid, cid FROM Community_User WHERE approved = 1)
	UNION
	(SELECT username, uid, cid from Moderator WHERE approved = 1)) as Users, Community WHERE Users.cid = Community.id AND Community.id = :cid AND Users.uid = :uid";
				$stm = $this->db->prepare($sql);
				$stm->execute(array(':cid' => $cid, ':uid' => $uid));
				return $stm->fetch();
			}
		}

		function insertUser($user, $arr, &$errorstring) {
			if (!is_numeric($user)) {
				$i = new IDinfo($this->db);
				$id = $i->getID($user);
				$user = $id['id'];

			}
			try {
					$sql = "SELECT * FROM Moderator WHERE username = :username AND cid = :cid";
					$stm = $this->db->prepare($sql);
					$stm->execute(array(':username'=> $arr['username'], ':cid' => $arr['cid']));
					$exists = $stm->fetch();
					if (!$exists) {
					$sql = "INSERT INTO Community_User (username, pass, cid, userid) VALUES (:username, :pass, :cid, :uid)";
					$stm = $this->db->prepare($sql);
					$stm->execute(array(':username' => $arr['username'], ':pass' => $arr['pass'], ':cid' => $arr['cid'], ':uid' => $user));
					$result = $arr['cid'];
					}
					else {
						$result = 0;
						$errorstring = "Username is taken. Pick another one.";
					}
				}


				catch (\PDOException $e) {
				if ($e->errorInfo[1] == 1062) {
					if ($res = $this->isUser($user, $arr['cid'])) {
						$errorstring = "You are already registered with this community! Your username is ". $res['username'];
					}
					else
						$errorstring = "Username is taken. Pick another one.";
					$result = 0;
    			}
			}
			return $result;
		}

	function verifylogin($arr) {
		$sql = "Select username, userid AS uid, cid, approved FROM Community_User WHERE username = :username AND pass = :pass AND cid= :cid";
		$stm = $this->db->prepare($sql);
		$stm->execute([':cid' => $arr['cid'], ':username' => $arr['username'], ':pass' => $arr['pass']]);
		$member = $stm->fetch();
		if ($member) {
			$member['mod'] = 0;
		}
		else {
			$sql = "Select username, uid, cid, approved FROM Moderator WHERE username = :username AND pass = :pass AND cid= :cid";
			$stm = $this->db->prepare($sql);
			$stm->execute([':cid' => $arr['cid'], ':username' => $arr['username'], ':pass' => $arr['pass']]);
			$member = $stm->fetch();
			if($member) {
			$member['mod'] = 1;
			}
			else {
				$member = 0;
			}
		}
		return $member;
	}

	function getFacultyCourseAvg($arr) {
		$sql = "SELECT * FROM (SELECT department, course_number, teacher, AVG(GPA) AS gpa FROM Course GROUP BY COURSE_NUMBER, TEACHER, DEPARTMENT) AS Something WHERE Something.teacher = :id AND Something.course_number = :num AND Something.department = :dept";
		$stm = $this->db->prepare($sql);
		$stm->execute([':id' => $arr['teacher'], ':num' => $arr['course_number'], ':dept' => $arr['department']]);
		return $stm->fetch();
	}

	function getFacultyCourseAvgHi($arr) {
		$sql = "SELECT a.department, a.course_number, b.GPA
FROM Course a
  INNER JOIN (
               SELECT department, course_number, MAX(GPA) AS GPA FROM (
                 (SELECT * FROM Course WHERE TEACHER = ? AND Course_Number = ? AND Department = ?) AS courses)
             ) b ON a.GPA = b.GPA";
		$stm = $this->db->prepare($sql);
		$stm->execute([$arr['teacher'], $arr['course_number'], $arr['department']]);
		return $stm->fetch();
	}

function getFacultyCourseAvgLo($arr) {
		$sql = "SELECT a.department, a.course_number, b.GPA
FROM Course a
  INNER JOIN (
               SELECT department, course_number, MIN(GPA) AS GPA FROM (
                 (SELECT * FROM Course WHERE TEACHER = ? AND course_number = ? AND department = ?) AS courses)
             ) b ON a.GPA = b.GPA";
		$stm = $this->db->prepare($sql);
		$stm->execute([$arr['teacher'], $arr['course_number'], $arr['department']]);
		return $stm->fetch();
	}

	function getCourseAvgHi($arr) {
		$sql = "SELECT department, course_number, MAX(GPA) AS GPA FROM (
                 (SELECT * FROM Course WHERE department = ? AND Course_Number = ?) AS courses)";
		$stm = $this->db->prepare($sql);
		$stm->execute([$arr['department'], $arr['course_number']]);
		return $stm->fetch();
	}

	function getCourseAvgLo($arr) {
		$sql = "SELECT department, course_number, MIN(GPA) AS GPA FROM (
                 (SELECT * FROM Course WHERE department = ? AND Course_Number = ?) AS courses)";
		$stm = $this->db->prepare($sql);
		$stm->execute([$arr['department'], $arr['course_number']]);
		return $stm->fetch();
	}
	function getFacultyAvgAll($uid) {
		$sql = "SELECT teacher, AVG(GPA) AS GPA FROM Course WHERE teacher = ?";
		$stm = $this->db->prepare($sql);
		$stm->execute([$uid]);
		return $stm->fetch();
	}
	function Linkify($cid, $text) {
		$string = "<a href=\"community.php?cid=$cid\">$text</a>";
		return $string;
	}
}
