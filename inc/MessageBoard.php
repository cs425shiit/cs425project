<?php
class MessageBoard{
  function __construct($pdo){
    $this->db = $pdo;
  }
  function getCommunity($discid){
    $sql = "SELECT * FROM Discussion WHERE id = $discid";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetch();
  }

  function getDiscussions($commid){
    $sql = "SELECT * FROM Discussion WHERE community_id = '$commid'";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetchAll();
  }

  function recent5($commid) {
	$sql = "Select Discussion.title, Discussion.id, User.uid, User.username, Forum_Comment.message, Forum_Comment.time_posted
FROM ((SELECT userid AS uid, username FROM Community_User WHERE cid = ?)
   UNION (SELECT uid, username FROM Moderator WHERE cid = ?)) AS User, Forum_Comment, Discussion, Community
WHERE User.uid = Forum_Comment.poster AND Discussion.community_id = Community.id
      AND Forum_Comment.discussion = Discussion.id ORDER BY time_posted DESC LIMIT 5";
	$stm = $this->db->prepare($sql);
	$stm->execute([$commid, $commid]);
	return $stm->fetchAll();
}

  function user5($userid) {
	$sql = "Select Community.name, Community.id as cid, Discussion.title, Discussion.id as did, User.uid, User.username, Forum_Comment.message, Forum_Comment.time_posted
FROM ((SELECT userid AS uid, username, cid FROM Community_User) UNION (SELECT uid, username, cid FROM Moderator)) AS User,
  Forum_Comment, Discussion, Community
WHERE User.uid = Forum_Comment.poster AND Discussion.community_id = Community.id AND User.cid = Community.id
      AND Forum_Comment.discussion = Discussion.id
      AND cid IN
          (SELECT cid
           FROM ((SELECT userid AS uid, username, cid FROM Community_User) UNION (SELECT uid, username, cid FROM Moderator)) AS User
           WHERE User.uid = ?)
ORDER BY time_posted DESC LIMIT 5";
	$stm = $this->db->prepare($sql);
	$stm->execute([$userid]);
	return $stm->fetchAll();
}

  function mostPopular() {
	$sql = "SELECT Community.name, Community.id, COUNT(*) AS comments
FROM Community, Discussion, Forum_Comment
WHERE Community.id = Discussion.community_id AND Discussion.id = Forum_Comment.discussion
GROUP BY Community.id
ORDER BY Comments DESC LIMIT 1";
	$stm = $this->db->prepare($sql);
	$stm->execute();
	return $stm->fetch();
}
  function getFirstComment($discid){
    $sql = "SELECT * FROM Forum_Comment WHERE discussion = $discid ORDER BY time_posted ASC";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetch();
  }

  function getDiscussionTitle($discid){
    $sql = "SELECT * FROM Discussion WHERE id = $discid";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetch()['title'];
  }

  function getLastComment($discid){
    $sql = "SELECT * FROM Forum_Comment WHERE discussion = $discid ORDER BY time_posted DESC";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetch();
  }

  function getComments($discid){
    $sql = "SELECT * FROM Forum_Comment WHERE discussion = '$discid'";
    $stm = $this->db->prepare($sql);
    $stm->execute();
    return $stm->fetchAll();
  }

  function postDiscussion($commid, $title, $e_arr){
	$this->db->beginTransaction();
    $sql = "INSERT INTO Discussion (title, community_id, started_by)
    VALUES (:title, $commid, :started_by)";
    $stm = $this->db->prepare($sql);
    $result = $stm->execute(array(':title' => htmlspecialchars($title), ':started_by' => $e_arr['poster']));
    $id = $this->db->lastInsertId();
    if($result){
      $sql = "INSERT INTO Forum_Comment (discussion, poster, message)
      VALUES ($id, :poster, :message)";
      $stm = $this->db->prepare($sql);
	  $this->db->commit();		
      return $stm->execute(array(':poster' => $e_arr['poster'], ':message' => htmlspecialchars($e_arr['message'])));
    }
	
    else {
		$this->db->rollBack();
		return 0;
	}
  }

  function postComment(array $e_arr){
    $sql = "INSERT INTO Forum_Comment (discussion, poster, message)
    VALUES (:discussion, :poster, :message)";
    $stm = $this->db->prepare($sql);
    return
    $stm->execute(array(':discussion' => $e_arr['discussion'],
     ':poster' => $e_arr['poster'], ':message' => $e_arr['message']));
  }
  function deleteDiscussion($discid){
    $sql = "DELETE FROM Discussion WHERE id = $discid";
    $stm = $this->db->prepare($sql);
    return $stm->execute();
  }
  function deletePost($postid){
    $sql = "UPDATE Forum_Comment SET message='This post has been deleted.' WHERE id = $postid";
    $stm = $this->db->prepare($sql);
    return $stm->execute();
  }
  function searchComm($commid, $message){
    $sql = "SELECT * FROM (SELECT community_id, id AS discid, title FROM Discussion) AS Discussion, Forum_Comment WHERE community_id = $commid AND discid = discussion AND (message LIKE ?) ORDER BY time_posted DESC";
	$wstring = "%" . $message . "%";
    $stm = $this->db->prepare($sql);
    $stm->execute([$wstring]);
    return $stm->fetchAll();
  }
  function searchDisc($discid, $message){
    $sql = "(SELECT discussion as did, time_posted, poster, message, Forum_Comment.id as id FROM Discussion, Forum_Comment WHERE Discussion.id = $discid AND discussion = $discid AND message LIKE ?)
	UNION (Select discussion as did, time_posted, poster, message, Forum_Comment.id as id FROM Discussion, Forum_Comment, ((SELECT username, userid as uid, cid FROM Community_User WHERE approved = 1)
	UNION
	(SELECT username, uid, cid from Moderator WHERE approved = 1)) AS User WHERE Discussion.id = discussion AND discussion = $discid AND poster = User.uid AND User.username LIKE ?) ORDER BY time_posted DESC";
	$wstring = "%" . $message . "%";
    $stm = $this->db->prepare($sql);
    $stm->execute([$wstring, $message]);
    return $stm->fetchAll();
  }

  function Linkify($cid, $did, $text){
		$string = "<a href=\"discussions.php?commid=$cid&discid=$did\">$text</a>";
		return $string;
  }
}

?>
