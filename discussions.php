<?php
// TO DO add "go back to all discussions for $commid" link
include 'inc/error.inc';
include 'inc/MessageBoard.php';
include 'inc/connect.inc';
include 'inc/IDinfo.php';
include 'inc/CommunityInfo.php';
$commid = $_GET['commid'];
$discid = $_GET['discid'];
$posted = $_GET['posted'];
$deletedisc = $_GET['deletedisc'];
$deletepost = $_GET['deletepost'];
$user = $_COOKIE[$commid];
$title = $_POST['title'];
$search = $_POST['search'];
$comm = new CommunityInfo($conn);
$mb = new MessageBoard($conn);
$id = new IDinfo($conn);
 
?>
<html>
<head><title>Discussions for <?php echo $cname = $comm->getCommunityfromID($commid)['name'];?></title></head>
<body>
 <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script> <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>
<?php include_once './inc/nav.html'; 
echo "<div class='title'><h2><span id='one'>" . $comm->Linkify($commid, $cname) . "</span>: " . $mb->Linkify($commid, '', 'Message Boards') . "</div><div class='container'>";
if(!$user) {
	echo "<div class='error'>This page is only accessible by members of this group. Perhaps you need to <a href='./community.php?cid=" . $commid . "'>log in.</a></div>";
}
else {
	if($commid){
	   $isMod = $comm->isMod($user, $commid);

	}

	if(!$commid && $discid)
	  $isMod = $comm->isMod($user, $mb->getCommunity($discid)['id']);

	if($isMod && $deletedisc){
	  $mb->deleteDiscussion($deletedisc);
	}

	if($isMod && $deletepost){
	  $mb->deletePost($deletepost);
	}

	if($posted == 1 && isset($_GET['discid'])){
	  if(isset($_POST['message']) && isset($_POST['poster']) && isset($_POST['discussion'])){
		$mb->postComment($_POST);
	  }
	}
	else if($posted == 1 && isset($_GET['commid'])){
	  if(isset($_POST['message']) && isset($_POST['poster']) && isset($_POST['discussion'])){
		$mb->postDiscussion($commid, $title, $_POST);
	  }

	}
		   $user = $comm->isUser($user,$commid);
	if($discid){

	  $comments = $mb->getComments($discid);
	    echo "<form id='discsearch' method=\"post\" action=" . htmlspecialchars($_SERVER['PHP_SELF']) . "?commid=$commid&discid=" . $discid .">";
	  echo "<input type=\"text\" name=\"search\" maxlength=\"499\" placeholder=\"Enter search\"> <br/>";
	  echo "<input type=\"submit\" value=\"Search!\">";
	  echo "</form>";
	  if($search){
		$sr = $mb->searchDisc($discid, $search);
		foreach($sr as $row){
		  echo "Discussion Title: " . $row['title'] . "<br>Posted By: " . $comm->getUser($row['poster'], $commid)['username'];
		  echo "<br>Message: " . htmlspecialchars_decode($row['message']) . "<br><hr><br>";
		}
	  }
		echo "<table style=\"table-layout: fixed; width:100%\"><colgroup><col style=\"width:200px\"></colgroup>";
	  echo "<h4>" . $mb->getDiscussionTitle($discid) . "</h4>";

	  foreach($comments as $row) {
		$d = new DateTime($row['time_posted']);
				$date = $d->format('M jS, Y<\b\\r>g:i a');
		echo "<tr><td><b>Posted By: </b>" . $id->Linkify($row['poster'],$comm->getUser($row['poster'], $commid)['username'])
			 . "</td><td><b>Time:</b> " .  $date . "</td>";
		echo $isMod ? "<td><a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?discid=$discid&deletepost=" . $row['id'] . "\">Delete?</a></td></tr>" : "</tr>";
			echo "<tr><td style=\"word-wrap: break-word\" colspan=\"2\">" . htmlspecialchars_decode($row['message']) . "</td></tr>";


		}
		echo "</table><p></p>\n";
 // POST TO DISCUSSION FORM

	  echo "<h3>Reply:</h3><br><form class ='discussion' method=\"post\" action= " . htmlspecialchars($_SERVER['PHP_SELF']) . "?commid=$commid&discid=" . $discid . "&posted=1>";
	  echo "<textarea name=\"message\" maxlength=\"499\" placeholder=\"Enter New Post Text Here\"></textarea> <br/>";
	  echo "<input type=\"hidden\" name=\"poster\" value=\"" . $user['uid'] . "\">";
	  echo "<input type=\"hidden\" name=\"discussion\" value=\"" . $discid . "\">";
	  echo "<input type=\"submit\" value=\"Post to Discussion\">";
	  echo "</form><p></p>";
//
		


	}
	else if($commid){
	  $discussions = $mb->getDiscussions($commid);
// SEARCH DISCUSSIONS FORM
	  echo "<form id='discsearch' method=\"post\" action=" . htmlspecialchars($_SERVER['PHP_SELF']) . "?commid=" . $commid .">";
	  echo "<input type=\"text\" name=\"search\" maxlength=\"499\" placeholder=\"Enter search\"> <br/>";
	  echo "<input type=\"submit\" value=\"Search!\">";
	  echo "</form>";
//
// POST NEW DISCUSSION FORM
	  echo "<div class='whisper'><a href=\"#newdisc\">Jump to bottom of page to add new discussion.</a></div><p></p><h2>Latest Discussions:</h2><p></p>";
// 
	  echo "<div id='disco'><table class='discussion'><tr>\n";
	  echo "<th>Title</th><th>Posted By</th><th>Last Post By</th><th>Updated</th></tr><tr><td colspan =\"4\"><hr></td></tr>";// display data
	  foreach($discussions as $row){
		$firstPost = $mb->getFirstComment($row['id']);
		$lastPost = $mb->getLastComment($row['id']);
		$d = new DateTime($lastPost['time_posted']);
				$date = $d->format('M jS, Y<\b\\r>g:i a');
	  	print "<tr><td>";
	  	echo "<a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?commid=$commid&discid=" . $row['id'] . "\">" . $row['title'] . "</a></td><td>";
	  	echo $id->Linkify($firstPost['poster'],$comm->getUser($firstPost['poster'], $commid)['username']) . "</td><td>";
	  	echo $id->Linkify($lastPost['poster'], $comm->getUser($lastPost['poster'], $commid)['username']) . "</td><td>";
	  	echo $date . "</td><td>\n";
		echo $isMod ? "<td><a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?commid=$commid&deletedisc=" . $row['id'] . "\">Delete?</a></td></tr>" : "</tr>";

	  }
		print "</table></div>\n";
// POST NEW DISCUSSION FORM
	    echo "<div id='newdisc'><form class='discussion' method=\"post\" action= " . htmlspecialchars($_SERVER['PHP_SELF']) . "?commid=" . $commid . "&posted=1>";
		echo "<input id='big' type=\"text\" name=\"title\" maxlength=\"249\" placeholder=\"Enter New Discussion Title Here\"> <br/>";
		echo "<textarea name=\"message\" maxlength=\"499\" placeholder=\"Enter New Post Text Here\"></textarea><br/>";
		echo "<input type=\"hidden\" name=\"poster\" value=\"" . $user['uid'] . "\">";
		echo "<input type=\"hidden\" name=\"discussion\" value=\"" . $discid . "\">";
		echo "<input type=\"submit\" value=\"Post New Discussion\">";
		echo "</form></div>";
// 

	  if($search){
		$sr = $mb->searchComm($commid, $search);
		foreach($sr as $row){
		  echo "Discussion Title: " . $mb->Linkify($row['community_id'], $row['discid'], $row['title']) . "<br>Posted By: " . $comm->getUser($lastPost['poster'], $commid)['username'];
		  echo "<br>Message: " . $row['message'];
		  echo "<br><hr><br>";
		}
	  }
	}
}
 ?>
</div>
</body>
</html>
