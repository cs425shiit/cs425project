<html>
<head>
<title>Verify Registration</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<div class="container">
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
$errors = 0;
$email = stripslashes($_POST['email']);
if ( $_POST['type'] == 'faculty') {
	if(stripos($email, 'iit.edu') == FALSE) {
		++$errors;
		echo "<div class='outter'><div class='error'>Please use your @iit.edu email.";
		echo "<br><a href=\"facultyregister.php\">return</a></div></div>";
		$email="";
	}
}
else if ($_POST['type'] == 'student') {
	if(stripos($email, 'hawk.iit.edu') == FALSE) {
		++$errors;
		echo "<div class='outter'><div class='error'>Please use your @hawk.iit.edu student email.";
		echo "<br><a href=\"register.php\">return</a></div></div>";
		$email="";
	}
}
else {
	echo "<div class='outter'><div class='error'>Something went wrong. <br><a href=\"index.php\">Go back</a></div></div>";

	++$errors;
}

if($errors == 0) {
try{
  include './inc/connect.inc';
  include './inc/IDinfo.php';
  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';

  $info = new IDinfo($conn);

  	$unseed = substr(strtolower(str_replace(' ', ' ', $_POST['last_name'])), 0, 7) . strval($_POST['start_year']);
	$unfinal = $unseed;
  	$otheruser = $info->getID($unseed);
  if($otheruser) {
	$x = 1;
	do {
		$un = $unseed . strval($x);
		$user = $info->getID($un);
		$x ++;
	} while($user);
        $unfinal = $un;
  }
  $pass = randomPassword();

	$err_string = "fff";
	$_POST['pass'] = $pass;
	$_POST['username'] = $unfinal;
	$success = $info->insertID($_POST, $err_string);
	if (!$success) {
		echo "<div class='error'>" . $err_string;
		echo "<br><a href=\"./index.php\">Go Back!</a></div>";
	}
	
	if ($success) {
			if($_POST['type'] == 'student') {
				$student = new StudentInfo($conn);
  				$done = $student->insertStudent($_POST, $success['id']);
				}
			else if ($_POST['type'] == 'faculty') {
				$faculty = new FacultyInfo($conn);
				$done = $faculty->insertFaculty($_POST, $success['id']);
				}	
		if ($done) {
			echo "<div class='outter'><div class='success'>Thank you for registering. <br>Your username is <div id='standout'>$unfinal</div> & your password is <div id='standout'>$pass</div><br>Please log in:</a></p></div>\n";
			include "./inc/loginscript.php";
		}
	}
		

  

  
}

catch (PDOException $e){
  print("Oops, something went wrong: " . $e->getMessage() . "<br/>");
}
}


function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass);
}
?></div></div>
</body>
</html>
