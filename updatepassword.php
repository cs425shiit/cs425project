<html>
<head><title>Update Password</title></head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>Update Password</h2></div><div class="container">
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
// get user ID from session/cookie
// check if user is faculty or student
// autopopulate form with their current selections
// update profile information/settings if submitted
  include './inc/connect.inc';
  include './inc/IDinfo.php';
$username = $_COOKIE["test"];
$p = new IDinfo($conn);
$user = $p->getID($username);
if(!$user) {
	echo "you must be logged in to do that.";
}
else {
	if (isset($_POST['changepass']) && ($_POST['password1'] <> $_POST['password2'])) {
		echo "Yikes, passwords don't match!";
	}
	else if (isset($_POST['changepass']) && ($_POST['password1'] == $_POST['password2'])) {
		$success = $p->updatePassword($username, $_POST['oldpassword'], $_POST['password1']);
		if ($success) {
			$user = $p->getID($username);
			echo "Your change has been submitted.";
			echo "<br><a href = \"./update.php\">Back to main settings page</a>";
		}
		else {
	    	echo "Cannot change password. The current password is incorrect.<br>";
			echo "<a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\">Try again</a>";
			echo "<br><a href = \"./update.php\">Back to main settings page</a>";
		}
	}
	else { ?>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<fieldset>
    <legend>Change password:</legend>
    Current Password:<br>
    <input type="password" name="oldpassword" required><br>
    New Password:<br>
    <input type="password" name="password1" required><br>
    New Password (again):<br>
    <input type="password" name="password2" required>
<br><br>
<input type="submit" value="submit changes" name="changepass">
</fieldset><br />
</fieldset>
</form><?php
 }}
?>
</div>
</body>
</html>
