<html>
<head><title>Approve Community Users</title></head>
<body>
<?php include_once './inc/nav.html';?>

<?php
include './inc/connect.inc';
include './inc/CommunityInfo.php';
include_once './inc/IDinfo.php';
$c = new CommunityInfo($conn);
$cid = $_GET['cid'];
$mod = $_COOKIE[$cid];
$i = new IDinfo($conn);
$unapproved_users = $c->getUnapprovedUsers($cid);
if(!$c->isMod($mod, $cid)) {
	echo "<div class='error'>Only moderators are able to access this information.<br>Perhaps you need to <a href=\"./community.php?cid=" . $cid . "\">log in</a>.";
}
else {
	echo "<div class=\"title\"><h2>Approve Users for " . $c->getCommunityfromID($cid)['name'] . "</h2></div><div class=\"container\">";
	if($unapproved_users) {
	echo "<table><tr><th>Name</th><th>Username</th><th></th><th></th></tr>";
	foreach ($unapproved_users as $user) {
		echo "<tr><td>" . $i->Linkify($user['uid'], $user['first_name'] . " " . $user['last_name']) . "</td><td>" . $user['nickname'] . "</td>";
		echo "<td><a href =\"./modApproveUser.php?uid=" . $user['id'] . "&cid=" . $cid . "\">approve</a></td>";
		echo "<td><a href =\"./modDeleteUser.php?uid=" . $user['id'] . "&cid=" . $cid . "\">delete</a></td></tr>";
	}
	echo "</table>";
	echo "<a href=\"./modApproveAllUsers.php?cid=" . $cid . "\">Approve All</a><br>";
	}
	else {
		echo "No one needs your approval at this time.<br>";
	}
	echo "<br><a href=\"./community.php?cid=" . $cid . "\">Return to Community Page</a>";
}
$conn = null;
?>
