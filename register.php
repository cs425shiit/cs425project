<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./inc/style.css">
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Oxygen">
<title>Student Registration</title>
<head>
<body>
<?php include_once "./inc/mast.html"; ?>
<div class="title"><h2>Student Registration</h2></div><div class="container">
<form method="post" action="verifyregistration.php">
<fieldset>
    <legend>Personal information:</legend>
    First name:<br>
    <input type="text" name="first_name" required><br>
    Last name:<br>
    <input type="text" name="last_name" required><br>
	email address:<br>
	<input type="email" name="email" required><div class="whisper">Hint: use your @hawk.iit.edu email.</div>
</fieldset><br />
<fieldset>
	<legend>Student information:</legend>
	Year you started at Illinois Tech:<br>
	<input type="number" name="start_year" min="1900" max="2016" required><br>
	Semester:
	<select name="start_semester">
		<option value="fall">Fall</option>
		<option value="spring">Spring</option>
		<option value="summer">Summer</option>
	</select><br>
	GPA:<br>
	<input type="number" name="GPA" min="0" max="4" size="3" step=".01" required<br>
	Degree Status: 
	<select name="degree_status">
		<option value="ongoing">ongoing</option>
		<option value="completed">completed</option>
		<option value="leave">leave of absence</option>
	</select><br><br>
	Degree Type: 
	<select name="degree_type">
		<option value="bachelors">Bachelor's</option>
		<option value="masters">Master's</option>
		<option value="phd">PhD</option>
	</select><br><br><br>
	<input type="hidden" name ="type" value="student">
	<input type="submit" value="register">
</fieldset>
</form>
</div>
</body>
</html>
