<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./inc/style.css">
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Oxygen">
<title>Faculty Registration</title>
</head>
<body>
<?php include_once "./inc/mast.html"; ?>
<div class="title"><h2>Faculty Registration</h2></div><div class="container">
<form method="post" action="verifyregistration.php">
<fieldset>
    <legend>Personal information:</legend>
    First name:<br>
    <input type="text" name="first_name" required><br>
    Last name:<br>
    <input type="text" name="last_name" required><br>
	email address:<br>
	<input type="email" name="email" required><div class="whisper">Hint: use your @iit.edu email."</div>
</fieldset><br />
<fieldset>
	<legend>Faculty information:</legend>
	Year you joined Illinois Tech:<br>
	<input type="number" name="start_year" min="1900" max="2016" required><br>
	Current position:<br>
	<input type="text" name="position" required maxlength="50">
	<input type="hidden" name="type" value="faculty">
	<br><br>
	<input type="submit" value="register">
</fieldset>
</form>
</div>
</body>
</html>
