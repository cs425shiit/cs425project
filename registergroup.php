<html>
<head>
  <title>Group Registration</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>Register Club / Group</h2></div><div class="container">
<?php 
  include './inc/connect.inc';
  include './inc/CommunityInfo.php';
  $username = $_COOKIE["test"];
if(!isset($username)) {
	echo "You must be logged in to that!";
	include "./inc/loginscript.php";
}
else {


if (isset($_POST['submit_group'])) {
	$c = new CommunityInfo($conn);
	if($_POST['type'] == 'club') {
		$success = $c->insertClub($_POST);
	}
	else {
		$success = $c->insertGroup($_POST);
	}
	if($success) {
		echo "Your group/club, " . $_POST['name'] . " has been submitted and is awaiting approval.";
	}
	else {
		echo "Something went wrong.";
	}
}	
?>
  <form name="group_registration_form" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <input type="text" name="name" placeholder="Enter Group Name"/>
    <br/><br/>
    <input type="radio" name="type" onclick="changeOptions(0);" value="club"/>
    Club
    <br/>
    <input type="radio" name="type" onclick="changeOptions(0);" value="interest"/>
    Interest Group
    <br/>
    <br/>
    <input type="submit" name="submit_group" value="register"/>
  </form>
<?php } ?>
</body>
</html>
