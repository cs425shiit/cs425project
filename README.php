<html>
<head>
  <title>
    CS425 Project README
  </title>
</head>
<body>
  <h1>CS 425 Project Walkthrough, Description and Test Data</h1>

<div id="ToC">
  <!--ToC will have 5 parts, they will be split into Page Navigation, Classes
and PHP functions, Tables, Testing Data and Support Files -->
  <h2>Table of Contents</h2>
  <ol>
    <li>Admin Pages</li>
    <ol>
      <li>adminApproveCourse.php</li>
      <li>adminDeleteCourse.php</li>
      <li>adminPage.php</li>
      <li>approveCourse.php</li>
      <li>approvegroups.php</li>
      <li>clearTable.php</li>

    </ol>

    <li>User Pages</li>
    <ol>
      <li>addexperiences.php</li>
    </ol>

    <li>Group Pages</li>
    <ol>
      <li>communities.php</li>
      <li>community.php</li>
    </ol>

    <li>Student Specific Pages</li>
    <ol>
      <li>changesettings.php</li>
    </ol>

    <li>Faculty Specific Pages</li>
    <ol>
      <li>facultyManageCourses.php</li>
    </ol>

    <li>Moderator Pages</li>
    <ol>
      <li>approveUsers.php</li>
    </ol>

    <li>Classes and Functions</li>
    <li>Tables</li>
    <li>Testing</li>
    <li>Support Files</li>
  </ol>
</div>
