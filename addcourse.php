<html>
<head>
<title>Courses - Hawkboard Faculty Site</title>
<style>
table th, td
{
	text-align:center;
}
table .lefta {
	text-align: left;
}


</style>
</head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>FACULTY PAGE: Add/Delete Courses</h2></div><div class="container">
<?php 
// TO DO - allow user to change GPA
// TO DO - display user's 
  include './inc/connect.inc';
  include './inc/CourseInfo.php';
  include './inc/FacultyInfo.php';
  $username = $_COOKIE["test"];
if(!isset($username)) {
	echo "You must be logged in to that!";
	include "./inc/loginscript.php";
}
else {
	$f = new FacultyInfo($conn);
	$teacher = $f->getFaculty($username);
}
if (!$teacher) {
	echo "Sorry. You do not have access to this page.";
}
else {
 	$c = new CourseInfo($conn);

if(isset($_POST['submitcourse']) && !isset($_POST['addmore'])) {
		empty($_POST['GPA']) ? null : $_POST['GPA'];
		$course = $c->insertCourse($_POST, $username);
		echo "Course has been recorded." ?>
		<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<input type="submit" name ="addmore" value = "add more"/>
		<?php } 
	else {?>
		<form id='courseadd' method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><table><tr><th>Course Name</th><th>Department</th><th>Course No</th><th>Year</th><th>Semester</th><th>Avg Grade<br>(if known)</th></tr>
<tr><td><input type="text" name="name" placeholder="(i.e. Calculus II)" required /></td><td>
<select id="department" name="department" size = "4" maxlength="4" required />
		<option value="AS">Air Force Aerospace Studies</option>
		<option value="ARCH">Architecture</option>
		<option value="AURB">Architecture and Urbanism</option>
		<option value="AAH">Art and Architectural History</option>
		<option value="BIOL">Biology</option>
		<option value="BME">Biomedical Engineering</option>
		<option value="BUS">Business</option>
		<option value="CHE">Chemical Engineering</option>
		<option value="CHEM">Chemistry</option>
		<option value="CAE">Civil and Architectural Engr</option>
		<option value="COM">Communications</option>
		<option value="CS">Computer Science</option>
		<option value="CSP">Computer Science Prof Master</option>
		<option value="COOP">Cooperative Education</option>
		<option value="ECON">Economics</option>
		<option value="ECE">Electrical and Computer Engr</option>
		<option value="EG">Engineering Graphics</option>
		<option value="EMGT">Engineering Management</option>
		<option value="ELP">English Language Program</option>
		<option value="ENVE">Environmental Engineering</option>
		<option value="EXCH">Exchange Student</option>
		<option value="FDSC">Food Science (CEUs only)</option>
		<option value="FDSN">Food Science and Nutrition</option>
		<option value="ENGR">General Engineering</option>
		<option value="GLS">General Learning Strategies</option>
		<option value="GCS">Graduate Continuation Studies</option>
		<option value="HIST">History</option>
		<option value="HUM">Humanities</option>
		<option value="IT-D">ITCP Development</option>
		<option value="IT-M">ITCP Management</option>
		<option value="IT-O">ITCP Operations</option>
		<option value="IT-S">ITCP Security</option>
		<option value="IT-T">ITCP Theory and Technology</option>
		<option value="ITMD">ITM Development</option>
		<option value="ITMM">ITM Management</option>
		<option value="ITMO">ITM Operations</option>
		<option value="ITMS">ITM Security</option>
		<option value="ITMT">ITM Theory and Technology</option>
		<option value="INTM">Industrial Tech and Mgmt</option>
		<option value="INT">Industrial Technology</option>
		<option value="ITM">Information Tech and Mgmt</option>
		<option value="IT">Information Technology</option>
		<option value="IDX">Institute of Design</option>
		<option value="IDN">Institute of Design</option>
		<option value="IPMM">Intellectual Prop Mgt and Mkts</option>
		<option value="IEP">Intensive English Program</option>
		<option value="INTR">Internship</option>
		<option value="IPRO">Interprofessional Project</option>
		<option value="LA">Landscape Architecture</option>
		<option value="LAW">Law</option>
		<option value="LCS">Law Continuation Studies</option>
		<option value="LIT">Literature</option>
		<option value="MBA">MBA Business</option>
		<option value="MSC">Management Science</option>
		<option value="MAX">Marketing Analytics</option>
		<option value="MSF">Master of Science in Finance</option>
		<option value="MS">Materials Science</option>
		<option value="MATH">Mathematics</option>
		<option value="MSED">Mathematics and Science Educ</option>
		<option value="MMAE">Mechl, Mtrls and Arspc Engrg</option>
		<option value="MILS">Military Science</option>
		<option value="NS">Naval Science</option>
		<option value="PHIL">Philosophy</option>
		<option value="PHYS">Physics</option>
		<option value="PS">Political Science</option>
		<option value="PCA">Prof Communication Advancement</option>
		<option value="PESL">Profcncy of Engl Second Lang</option>
		<option value="PD">Professional Development</option>
		<option value="PL">Professional Learning</option>
		<option value="PSYC">Psychology</option>
		<option value="PA">Public Administration</option>
		<option value="SCI">Science</option>
		<option value="SHMR">Shimer College</option>
		<option value="SSCI">Social Sciences</option>
		<option value="SOC">Sociology</option>
		<option value="SSB">Stuart School of Business</option>
		<option value="STDA">Study Abroad</option>
		<option value="SMGT">Sustainability Management</option>
		<option value="TECH">Technology</option>
		<option value="UCS">Undergrad Continuing Studies</option>
		</select></td><td><input type="number" placeholder="123" name="course_number" min="100" max="999"/></td>
		<td>
		<input type="number" placeholder="Year" name="year" min="1900" max="2020" required>
		</td>
		<td><select name="semester">
		<option value="fall">Fall</option>
		<option value="spring">Spring</option>
		<option value="summer">Summer</option>
		</select></td>
		<td>
		<input type="number" placeholder="x.xx" valuename="GPA" min="0" max="4" size="3" step=".01"></td></tr>
		<tr><td>
  		<input type="submit" name="submitcourse" value="add course"/></td></tr></table>
		</form>
		Your Courses:<br>
<?php $allcourses = $c->getCoursesByFaculty($username);
	echo "<table id='facultycourse'><tr>\n";
	// add the table headers
	echo "<th>Name</th><th>Course No.</th><th>Semester</th><th>Year</th><th>Avg Grade</th><th>Approved?</th><th>Delete</th></tr></tr><tr><td colspan =\"7\"><hr></td></tr>";// display data
	foreach ($allcourses as $row) {
    	print "<tr><td class=\"lefta\">";
    	echo $row['name'] . "</td><td>";
		echo $row['department'] . $row['course_number'] . "</td><td>";
		echo $row['semester'] . "</td><td>";
		echo $row['year'] . "</td><td>";
		echo $row['GPA'] . "</td>";
		echo "<td>";
		if ($row['approved'])
			echo  "&#x2714</td><td></td>";
		else
			echo "</td><td><a href =\"./deleteCourse.php?cid=" . $row['id'] . "\">&#x2716</a></td>";
        echo "</tr><tr><td colspan =\"7\"><hr></td>\n";
    	}

	
	// close the table
	print "</table>\n";
} 

} $conn = null;
?>
 
</body>
</html>

