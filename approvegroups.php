<?php
include_once './inc/connect.inc';
include_once './inc/CommunityInfo.php';
echo "<h3>COMMUNITIES TO BE APPROVED</h3>";
$c = new CommunityInfo($conn);
$unapproved = $c->getUnapprovedCommunities();
if (!$unapproved) {
	echo "No communities awaiting approval at this time.";
}
else {
	echo "<table><tr>\n";
		// add the table headers
	echo "<th>Name</th><th>type</th><th>&#x2714;</th><th>&#x2718;</th></tr><tr><td colspan =\"4\"><hr></td></tr>";// display data

	foreach($unapproved as $row){
		print "<tr><td>";
		echo $row['name'] . "</td><td>";
		echo $row['type'] . "</td>";
	  echo "<td><a href =\"./adminApproveCourse.php?cid=" . $row['id'] . "\">approve</a></td>";
		echo "<td><a href =\"./adminDeleteCourse.php?cid=" . $row['id'] . "\">delete</a></td>";
	  echo "</tr><tr><td colspan =\"4\"><hr></td>\n";

		}
	print "</table>\n";
}
?>
