<html>
<head><title>Approve Community Users</title></head>
<body>
<?php include_once './inc/nav.html';?>
<?php
include './inc/connect.inc';
include './inc/CommunityInfo.php';

$comm = new CommunityInfo($conn);

$uid = $_GET['uid'];
$cid = $_GET['cid'];
echo "<div class=\"title\"><h2>Approve Users for " . $comm->Linkify($cid, $comm->getCommunityfromID($cid)['name']) . "</h2></div><div class=\"container\">";
$success = $comm->deleteUser($cid, $uid);

if($success){
    echo "Users successfully deleted! ";
}
else{
    echo "<div class='error'>Something went wrong deleting user.</div>";
}

echo "<a href='./approveUsers.php?cid=$cid'>Return to Mod Page</a>";

?>
</div>
</body>
</html>
