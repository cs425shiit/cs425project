<html>
<head>
<title>Admin: Delete Course/Community</title>
</head>
<body>
<?php include_once './inc/nav.html';?>
<?php
   include './inc/connect.inc';
  include './inc/CommunityInfo.php';
  include './inc/CourseInfo.php';


  $c = new CommunityInfo($conn);

  $cid = $_GET["cid"];
  $comm = $c->getCommunityfromID($cid);
  $k = new CourseInfo($conn);
  
if($comm['type'] == 'course') {
echo "<div class='title'><h2>Delete Course</h2></div>";
	if(isset($_POST['yes'])) {


			$success = $k->deleteCourse($_POST['cid']);
			if($success) {
				echo "Course has been deleted! <a href =\"./approveCourse.php\">Return to your POWER!</a>";
			}
			else {
				echo "Something went wrong. Course could not be deleted. <a href =\"./approveCourse.php\">Return to your POWER!</a>";
			}
		}
	else {
	?>
	  Are you sure you want to delete this course?<br>
	<?php
			echo "<table width=\"100%\" border=\"0|0\"><tr>\n";
			// add the table headers
			echo "<th>Name</th><th>Dept</th><th>Course No.</th><th>Semester</th><th>Year</th></tr><tr><td colspan =\"5\"><hr></td></tr>";// display data
			print "<tr><td>";
			echo $comm['name'] . "</td><td>";
			echo $course['department'] . "</td><td>";
			echo $course['course_number'] . "</td><td>";
			echo $course['semester'] . "</td><td>";
			echo $course['year'] . "</td><td>";
			echo $course['GPA'] . "</td>";
		    echo "</tr><tr><td colspan =\"5\"><hr></td><tr></table>\n";
			echo "<form method=\"POST\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\"><input type = \"hidden\" name =\"courseuser\" value=\"" . $course['teacher'] . "\">";
			echo "<input type=\"hidden\" name =\"cid\" value = \"" . $cid . "\"><input type=\"submit\" value=\"yes\" name=\"yes\"><a href =\"./approvegroups.php\">No, take me back!</a></form>";
	}
}
else {
echo "<div class='title'><h2>Delete Community</h2></div>";
	if(isset($_POST['yes'])) {

	
		

			$success = $c->deleteCommunity($_POST['cid']);
			if($success) {
				echo "Community has been deleted! <a href =\"./approvegroups.php\">Return to your POWER!</a>";
			}
			else {
				echo "Something went wrong. Community could not be approved. <a href =\"./approvegroups.php\">Return to your POWER!</a>";
			}
	}
	
	else {
	?>
	  Are you sure you want to delete this community?<br>
	<?php
			
			echo "<table width=\"100%\" border=\"0|0\"><tr>\n";
			// add the table headers
			echo "<th>Name</th><th>Type</th></tr><tr><td colspan =\"2\"><hr></td></tr>";// display data
			print "<tr><td>";
			echo $comm['name'] . "</td><td>";
			echo $comm['type'] . "</td><td>";
		    echo "</tr><tr><td colspan =\"5\"><hr></td><tr></table>\n";
			echo "<form method=\"POST\" action=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\"><input type = \"hidden\" name =\"courseuser\" value=\"" . $course['teacher'] . "\">";
			echo "<input type=\"hidden\" name =\"cid\" value = \"" . $cid . "\"><input type=\"submit\" value=\"yes\" name=\"yes\"><a href =\"./approvegroups.php\">No, take me back!</a></form>";
	}


}
?>


</body>
</html>
