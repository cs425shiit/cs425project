<?php
  function generatePages($name){
    include './inc/connect.inc';
    include './inc/error.inc';

    $sql = "SELECT *
    FROM Student, ID, Settings
    WHERE ID.id = Student.id AND ID.username = '$name' AND ID.id = Settings.id";

    foreach($conn->query($sql) as $row){
      $email = $row['email'];
      $first_name = $row['first_name'];
      $last_name = $row['last_name'];
      $start_year = $row['start_year'];
      $start_semester = $row['start_semester'];
      $GPA = $row['GPA'];
      $degree_status = $row['$degree_status'];
      $degree_type = $row['degree_type'];
      $is_TA = $row['is_TA'];
      $show_start_semester_and_year = $row['show_start_semester_and_year'];
      $show_degree_status = $row['show_degree_status'];
      $show_degree_type = $row['show_degree_type'];
      $show_GPA = $row['show_GPA'];
      $show_following = $row['show_following'];

      $start_semester_and_year_output = $show_start_semester_and_year ?
      "Start Semester: $start_semester<br/>Start Year: $start_year<br/>":"";

      $degree_status_output = $show_degree_status?"Degree Status: $degree_status <br/>":"";
      $degree_type_output = $show_degree_type?"Degree Type: $degree_type<br/>":"";
      $GPA_output =  $show_GPA?"GPA: $GPA </br>":"";

      ob_start();
      require_once('./templates/profile_my.php');
      $newMyContent = ob_get_clean();

      ob_start();
      require_once('./templates/profile.php');
      $newPubContent = ob_get_clean();

      $file = "./profiles/$name" . "_my.php";
      $fp = fopen($file, 'w');
      chmod($file, 0777);

      fwrite($fp, $newMyContent);
      sleep(1);
      fclose($fp);

      $file ="./profiles/$name" . ".php";
      $fp = fopen($file, 'w');
      chmod($file, 0777);
      fwrite($fp, $newPubContent);
      sleep(1);
      fclose($fp);
    }
}
?>
