<html>
<head><title>Communities</title></head>
<body>
<?php include_once './inc/nav.html'; ?>
<div class='title'><h2>Search for Communities</h2></div><div class="container">

<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
include './inc/connect.inc';
include './inc/CommunityInfo.php';
include './inc/CourseInfo.php';
include './inc/Search.php';
include './inc/MessageBoard.php';
$username = $_COOKIE["test"];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else { 
	echo "<a href=\"./moderators.php\">See All Moderators</a><br>";
	echo "Find a club or group: ";
?>
<div class="whisper">or <a href="/cs425project/registergroup.php">create a new one</a></div>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><input type="text" placeholder = "enter full or partial name" name="query"><input type="submit" name ="search" value = "search"/></form>

<?php
	if(isset($_POST['search'])) {
		$search = new Search($conn);
		$count = 0;
		$results = $search->getGroupsLike($_POST['query'], $count);
		if(!$count) {
			echo "Sorry, we couldn't find them! Try using a different spelling or searching a partial name!";
		}
		else {
			echo "$count results:<br>";
			foreach ($results as $row) {
				$path = "/cs425project/community.php?cid=" . $row['id'];
    			echo "<a href=\"" . $path . "\">" . $row['name'] . "</a><br>";
    		} 
		}
	}
?>
<br>Show me...
<form method="POST" action ="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><button name="course">all courses</button> <button name="group">all groups</button> <button name="club">all clubs</button>
<p><?php
$g = new CommunityInfo($conn);
$c = new CourseInfo($conn);

if (isset($_POST['course'])) {
	echo "<h3>courses</h3>";
	$courses = $c->getCourses();
	echo "<table>";
	foreach ($courses as $row) {
		echo "<tr><td><a href=\"community.php?cid=" . $row['id'] . "\">" . $row['name'] . "</a></td>";
		echo "<td>" . $row['department'] . $row['course_number'] . "</td>";
		echo "</tr><tr><tr>";
		echo "<td>" . ucwords($row['first_name']) .  " " . ucwords($row['last_name']) . "</td><td>" . ucwords($row['semester']) . " " . $row['year'];
	}
	echo "</table>";
	
	}
	else if (isset($_POST['group'])) {
		echo "<h3>groups</h3><table>";
		$groups = $g->getGroups();
		foreach ($groups as $row) {
			echo "<tr><td><a href=\"community.php?cid=" . $row['id'] . "\">" . $row['name'] . "</a></td>";
		}
		echo "</table>";

	}
	else if (isset($_POST['club'])) {
		echo "<h3>clubs</h3>";
		$clubs = $g->getClubs();
		echo "<table>";
		foreach ($clubs as $row) {
			echo "<tr><td><a href=\"community.php?cid=" . $row['id'] . "\">" . $row['name'] . "</a></td>";
		}
	}
	$mb = new MessageBoard($conn);
	$popular = $mb->mostPopular();
	echo "With " . $popular['comments'] . " comments, " . $g->Linkify($popular['id'], $popular['name']) . " is our most popular community!";
}
?>
</p></div>
</body>
</html>
