<html>

<title>Display Table</title>
<h1>Display results for <?php echo htmlspecialchars($_POST['table']); ?></h1>

<?php
include './inc/connect.inc';

// Check connection
if ($conn->errorCode() == false) {
    die("Connection failed: " . $conn->errorCode());
}

$sql = "SELECT * FROM " . $_POST['table'];
$result = $conn->query($sql);
$result->setFetchMode(PDO::FETCH_ASSOC);

$sql2 = "SHOW COLUMNS FROM " . $_POST['table'];
$result2 = $conn->query($sql2);
$result2->setFetchMode(PDO::FETCH_ASSOC);

if ($result->rowCount() > 0) {
    echo "<table border='1'>";
    // output data of each row
    while($row = $result->fetch()) {

        echo "<tr>";

        while($cols = $result2->fetchColumn()){
            echo "<td>".$cols."</td>";
        }

        echo "</tr>";
        echo "<tr>";

        foreach($row as $value)
        {
            echo "<td>".$value."</td>";
        }

        echo "</tr>";

    }
    echo "</table>";
} else {
    echo "0 results";
}?>
<p>Enter Data into the table</p>
<form action="insertData.php" method="post">
    <fieldset>
        <?php
        $sql = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" . $_POST['table'] . "';";
        $col_types = $conn->query($sql);
        $col_types->setFetchMode(PDO::FETCH_ASSOC);

        $sql2 = "SHOW COLUMNS FROM " . $_POST['table'];
        $result2 = $conn->query($sql2);
        $result2->setFetchMode(PDO::FETCH_ASSOC);

        $count = 0;

        echo "<input type='hidden' name='table' value='" . $_POST['table'] . "' />";
        while($cols = $result2->fetchColumn()){
            $type = $col_types->fetchColumn();
            if ($type == "int"){
                echo "<input type='number' name='$cols'>$cols<br>";
            } elseif ($type == "varchar"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "tinyint"){
                echo "<input type='number' name='$cols'>$cols<br>";
            } elseif ($type == "bigint"){
                echo "<input type='number' name='$cols'>$cols<br>";
            } elseif ($type == "longtext"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "datetime"){
                echo "<input type='datetime' name='$cols'>$cols<br>";
            } elseif ($type == "decimal"){
                echo "<input type='number' step='any' name='$cols'>$cols<br>";
            } elseif ($type == "float"){
                echo "<input type='number' step='any' name='$cols'>$cols<br>";
            } elseif ($type == "double"){
                echo "<input type='number' step='any' name='$cols'>$cols<br>";
            } elseif ($type == "year"){
                echo "<input type='number' name='$cols'>$cols<br>";
            } elseif ($type == "date"){
                echo "<input type='date' name='$cols' min='1900-01-01'>$cols Date<br>";
            } elseif ($type == "timestamp"){
                echo "<input type='time' name='$cols'>$cols<br>";
            } elseif ($type == "char"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "set"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "enum"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "longblob"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "mediumtext"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "mediumblob"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "smallint"){
                echo "<input type='number' name='$cols'>$cols<br>";
            } elseif ($type == "text"){
                echo "<input type='text' name='$cols'>$cols<br>";
            } elseif ($type == "blob"){
                echo "<input type='text' name='$cols'>$cols<br>";
            }

            echo "<input type='hidden' name='arg" . $count . "' value='" . $cols . "' />";
            $count++;

        }
        ?>
        <input type="submit" value="Insert"/>
    </fieldset>
</form>
<form action="clearTable.php" method="post">
    <fieldset>
        <input type="hidden" value="<?php echo $_POST['table'] ?>" name="table" \>
        <input type="submit" value="Clear Table" onclick="return confirm('WARNING: This will delete ALL data in the table. Are you sure?')" />
    </fieldset>
</form>
<form action="adminPage.php">
    <fieldset>
        <input type="submit" value="Return to Admin Page" />
    </fieldset>
</form>
</html>