<html>

<?php
include './inc/connect.inc';

$sql = "INSERT INTO" . " " . $_POST['table'] . "(";
$count = 0;

foreach($_POST as $arg){
    if ($count == 2){
        $sql = $sql . $arg;
    } elseif ($count > 2 && $count%2 == 0){ # Add && $arg !== "id" when ID Auto Increments
        $sql = $sql . ", " . $arg;
    }
    $count++;
}

$sql = $sql . ") VALUES(";

$count = 0;

foreach($_POST as $arg){
    if ($count == 1){
        if (gettype($arg) == "string"){
            $sql = $sql . "'" . $arg . "'";
        } else{
            $sql = $sql . $arg;
        }

    } elseif ($count > 1 && $count%2 == 1){ #Add && $count !== $id when ID Auto Increments
        if (gettype($arg) == "string"){
            $sql = $sql . ", '" . $arg . "'";
        } else{
            $sql = $sql . ", " . $arg;
        }
    }
    $count++;
}
$sql = $sql . ");";

echo $sql;

$result = $conn->prepare($sql);

if (!$result){
    echo "<br> Error preparing query: " . $conn->errorInfo() . " Click return to be redirected to Display Table<br>";
} else {
    try{
        $result->execute();
        echo "<br> Completed Successfully. Click return to be redirected to Display Table. <br>";
    } catch(PDOException $e) {
        echo "<br> Error executing query: $e <br> Click return to be redirected to Display Table";
    }

}
?>
<form action="displayTable.php" method="post">
    <fieldset>
        <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>" />
        <input type="submit" value="Return">
    </fieldset>
</form>
</html>