<html>
<?php
include './inc/connect.inc';

$sql = file_get_contents('./sql/clearAll.sql');

$result = $conn->prepare($sql);

if ($result){
    try{
        $result->execute();
        echo "Data was cleared successfully. ";
    }
    catch (PDOException $e){
        echo "Oops: something went wrong. " . $e;
    }
}
else{
    echo "Oops: Something went wrong. " . $conn->errorInfo();
}

?>
</br>
<a href="./adminPage.php">Return to AdminPage</a>
</html>