# Q1
Select Discussion.title, User.uid, User.username, Forum_Comment.message, Forum_Comment.time_posted
FROM ((SELECT userid AS uid, username FROM Community_User)
   UNION (SELECT uid, username FROM Moderator)) AS User, Forum_Comment, Discussion, Community
WHERE User.uid = Forum_Comment.poster AND Discussion.community_id = Community.id
      AND Forum_Comment.discussion = Discussion.id AND Community.id = x ORDER BY time_posted DESC LIMIT 5;

# Q2
Select Community.name, Discussion.title, User.uid, User.username, Forum_Comment.message, Forum_Comment.time_posted
FROM ((SELECT userid AS uid, username FROM Community_User) UNION (SELECT uid, username FROM Moderator)) AS User,
  Forum_Comment, Discussion, Community
WHERE User.uid = Forum_Comment.poster AND Discussion.community_id = Community.id
      AND Forum_Comment.discussion = Discussion.id
      AND Community.id IN
          (SELECT Community.id
           FROM Community_User, Community, Moderator
           WHERE (Community_User.cid = Community.id OR Moderator.cid = Community.id)
                 AND (Community_User.userid = ? OR Moderator.uid = ?))
ORDER BY time_posted DESC LIMIT 5

# Q6
SELECT Community.name, Community.id, COUNT(*) AS Comments
FROM Community, Discussion, Forum_Comment
WHERE Community.id = Discussion.community_id AND Discussion.id = Forum_Comment.discussion
GROUP BY Community.id
ORDER BY Comments DESC LIMIT 1
