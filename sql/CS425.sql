-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2016 at 11:55 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `CS425`
--
CREATE DATABASE IF NOT EXISTS `CS425` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `CS425`;

-- --------------------------------------------------------

--
-- Table structure for table `Community`
--

CREATE TABLE IF NOT EXISTS `Community` (
  `name` varchar(250) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE IF NOT EXISTS `Course` (
  `department` varchar(30) NOT NULL,
  `course_number` int(3) NOT NULL,
  `teacher` int(10) unsigned DEFAULT NULL,
  `GPA` float DEFAULT NULL,
  `semester` varchar(6) NOT NULL,
  `year` year(4) NOT NULL,
  `approved` tinyint(1) DEFAULT '0',
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Experience`
--

CREATE TABLE IF NOT EXISTS `Experience` (
  `id` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `position` varchar(30) DEFAULT NULL,
  `company` varchar(30) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Faculty`
--

CREATE TABLE IF NOT EXISTS `Faculty` (
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `start_year` int(4) NOT NULL,
  `position` varchar(50) NOT NULL,
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ID`
--

CREATE TABLE IF NOT EXISTS `ID` (
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Moderator`
--

CREATE TABLE IF NOT EXISTS `Moderator` (
  `id` int(10) unsigned DEFAULT NULL,
  `community` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Settings`
--

CREATE TABLE IF NOT EXISTS `Settings` (
  `id` int(10) unsigned NOT NULL,
  `show_start_semester_and_year` tinyint(1) NOT NULL DEFAULT '0',
  `show_degree_status` tinyint(1) NOT NULL DEFAULT '0',
  `show_degree_type` tinyint(1) NOT NULL DEFAULT '0',
  `show_GPA` tinyint(1) NOT NULL DEFAULT '0',
  `show_following` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Student`
--

CREATE TABLE IF NOT EXISTS `Student` (
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `start_year` int(4) NOT NULL,
  `start_semester` varchar(6) NOT NULL,
  `GPA` float NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `degree_status` varchar(11) NOT NULL,
  `degree_type` varchar(25) NOT NULL,
  `is_TA` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `Student`
--
DROP TRIGGER IF EXISTS `createSettings`;
DELIMITER //
CREATE TRIGGER `createSettings` AFTER INSERT ON `Student`
 FOR EACH ROW INSERT INTO Settings( id ) VALUES (NEW.id)
//
DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
