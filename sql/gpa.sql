SELECT * FROM COURSE;

# Average GPA for a teacher across all courses
SELECT teacher, AVG(GPA) AS GPA FROM COURSE GROUP BY TEACHER;

# Maximum average GPA for course based on the Teacher
SELECT a.DEPARTMENT, a.COURSE_NUMBER, a.TEACHER, b.GPA
FROM (SELECT department, course_number, teacher, AVG(GPA) AS GPA FROM COURSE GROUP BY COURSE_NUMBER, TEACHER, DEPARTMENT) a
  INNER JOIN (
               SELECT department, course_number, teacher, MAX(GPA) AS GPA FROM (
                 (SELECT department, course_number, teacher, AVG(GPA) AS GPA
                  FROM COURSE GROUP BY COURSE_NUMBER, TEACHER, DEPARTMENT) AS average)
               GROUP BY COURSE_NUMBER, DEPARTMENT
             ) b ON a.GPA = b.GPA;

# Same, but gets minimum
SELECT a.DEPARTMENT, a.COURSE_NUMBER, a.TEACHER, b.GPA
FROM (SELECT department, course_number, teacher, AVG(GPA) AS GPA FROM COURSE GROUP BY COURSE_NUMBER, TEACHER, DEPARTMENT) a
  INNER JOIN (
               SELECT department, course_number, teacher, MIN(GPA) AS GPA FROM (
                 (SELECT department, course_number, teacher, AVG(GPA) AS GPA
                  FROM COURSE GROUP BY COURSE_NUMBER, TEACHER, DEPARTMENT) AS average)
               GROUP BY COURSE_NUMBER, DEPARTMENT
             ) b ON a.GPA = b.GPA;

# Maximum GPA for a teacher
SELECT a.department, a.course_number, b.GPA
FROM COURSE a
  INNER JOIN (
               SELECT department, course_number, MAX(GPA) AS GPA FROM (
                 (SELECT * FROM COURSE WHERE TEACHER = 85) AS courses)
             ) b ON a.GPA = b.GPA;

# Minimum GPA for a teacher
SELECT a.department, a.course_number, b.GPA
FROM COURSE a
  INNER JOIN (
               SELECT department, course_number, MIN(GPA) AS GPA FROM (
                 (SELECT * FROM COURSE WHERE TEACHER = 85) AS courses)
             ) b ON a.GPA = b.GPA;