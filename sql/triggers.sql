CREATE TRIGGER createSettings 
AFTER INSERT ON Student
	FOR EACH ROW
	INSERT INTO Settings( id ) VALUES (NEW.id) ;


DELIMITER $$
CREATE TRIGGER `TA_not_student`
BEFORE INSERT ON `TA` FOR EACH ROW
BEGIN
	IF EXISTS (SELECT C.department, C.course_number, C.id FROM ((Select A.department, A.course_number, A.id FROM Course A LEFT JOIN (SELECT * FROM Course WHERE id=1) B ON A.id <> B.id WHERE A.course_number = B.course_number AND A.department = B.department) UNION (select department, course_number, id FROM Course WHERE id= NEW.cid)) as C, Community_User WHERE Community_User.cid = C.id AND Community_User.userid = NEW.id)
	THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Cannot add student as TA of a course they are enrolled in.';
END IF;
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `TA_not_student_update`
BEFORE UPDATE ON `TA` FOR EACH ROW
BEGIN
	IF EXISTS (SELECT C.department, C.course_number, C.id FROM ((Select A.department, A.course_number, A.id FROM Course A LEFT JOIN (SELECT * FROM Course WHERE id=1) B ON A.id <> B.id WHERE A.course_number = B.course_number AND A.department = B.department) UNION (select department, course_number, id FROM Course WHERE id= NEW.cid)) as C, Community_User WHERE Community_User.cid = C.id AND Community_User.userid = NEW.id)
	THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Cannot add student as TA of a course they are enrolled in.';
END IF;
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER `Non_TA_Not_Mod`
BEFORE INSERT ON `Moderator` FOR EACH ROW
BEGIN
	IF EXISTS(SELECT 1 FROM Community WHERE type = 'course' AND id = NEW.cid) AND NOT EXISTS (SELECT 1 FROM TA WHERE id = NEW.uid AND cid = NEW.cid) AND NOT EXISTS (SELECT 1 FROM Course WHERE Course.teacher = NEW.uid AND Course.id = NEW.cid) 
	THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Only TAs and Faculty may moderate courses';
	END IF;
		IF ((SELECT COUNT(cid) FROM Moderator, Community where cid = NEW.cid AND cid = id AND Community.type != 'course') > 1) THEN 
			 
           SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = "ERROR: Cannot have more than 2 moderators.";
		END IF;
END$$
DELIMITER ;


DELIMITER $$
CREATE TRIGGER make_teacher_moderator
AFTER UPDATE ON Course
	FOR EACH ROW
	BEGIN
		INSERT INTO Moderator (cid, uid) VALUES (NEW.id, NEW.teacher);
	END$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER check_num_mods
BEFORE INSERT ON Moderator
	FOR EACH ROW
    BEGIN
		DECLARE msg VARCHAR(50);
		IF ((SELECT COUNT(cid) FROM Moderator, Community where cid = NEW.cid AND cid = id AND Community.type != 'course') > 1) THEN 
			set msg = "ERROR: Cannot have more than 2 moderators.";
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
		END IF;
	END$$
DELIMITER ;

DELIMITER $$
CREATE TRIGGER update_TA
AFTER INSERT ON TA
	FOR EACH ROW
    BEGIN
		UPDATE Student SET is_TA = TRUE WHERE NEW.id = STUDENT.id;
	END$$
DELIMITER ;
