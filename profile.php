<html>
<head><title>Profile</title></head>
<body>
<?php include_once './inc/nav.html'; ?>
<?php
// if user isn't logged in -> you gotta login, bro
// if there is no get or get isn't valid -> go to search
// else use GET to get the info and show the profile
// if user is looking at their own profile -> this is your profile as seen by the world, edit settings here
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
  include './inc/SettingsInfo.php';
  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';
  include './inc/CommunityInfo.php';
  include './inc/CourseInfo.php';
  include './inc/ExperienceInfo.php';
  include_once './inc/IDinfo.php';
$username = $_COOKIE["test"];
$i = new IDinfo($conn);
$visitor = $i->getID($username);
$profile = $_GET['uid'];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else {
if($profile == $visitor['id'])
			echo "<div class='whisper'>This is your public profile. Change your settings <a href=\"update.php\">here</a>.</div>";
	$s = new StudentInfo($conn);
	$f = new FacultyInfo($conn);
	if ($student = $s->getStudent($profile)) {
		

	// breaks when made into a function for some reason.
		$set = new SettingsInfo($conn);
		$settings = $set->getSettings($profile);
		$i = new IDinfo($conn);
		$id = $i->getID($profile);
		$p_arr = array_merge($settings,$student,$id);
	// -------------------------------------------------

		$start_semester_and_year_output = $p_arr['show_start_semester_and_year'] ?
			"<i>joined " . ucfirst($p_arr['start_semester']) . " "
				. $p_arr['start_year'] . "</i><br/>" : "";
		$degree_status_output = $p_arr['show_degree_status']?"Degree Status: "
				. $p_arr['degree_status'] . "<br/>":"";
		  $degree_type_output = $p_arr['show_degree_type']?"Degree Type: "
				. ucfirst($p_arr['degree_type']) . "<br/>":"";
		  $GPA_output =  $p_arr['show_GPA']?"GPA: " . number_format($p_arr['GPA'], 2) . " <br/>":"";

		echo "<div class='title'><h2>" . ucwords($p_arr['first_name']) . " " . ucwords($p_arr['last_name']) . "</h2></div><div class=\"container\"><div id='profilecontent'><div id='profileleft'>" .
		$start_semester_and_year_output
		. $degree_status_output . $degree_type_output . $GPA_output
		. "<a href=\"mailto:" . $p_arr['email'] . "\">email " . ucwords($p_arr['first_name']) . "</a><p></p>";
		echo "<h3>Jobs, Projects & Experiences</h3>";
			$username = $profile;
			$e = new ExperienceInfo($conn);
			$alljobs = $e->getExperiences($username);
			
			// add the table headers
			echo "</tr>";// display data
			if (empty($alljobs[0])) {
				echo "No jobs, projects, or experiences added yet!<br>";
			}
			else {
				echo "<table><tr>\n";
				foreach ($alljobs as $row) {
					print "<tr><td>";
					echo $row['company'] . "</td><td><i>";
					echo $row['type'] . "</i></td><td>";
					$date = new DateTime($row['start_date']);
					echo date_format($date, "m/j/Y") . "</td>";
					if ($row['end_date'] != "0000-00-00") {
						$edate = new DateTime($row['end_date']);
						echo "<td>" . date_format($edate, "m/j/Y") . "</td>";
					}
					else {
						echo "<td>present</td>";
					}
					echo "</tr><tr><td>";
					echo $row['position'] . "</td><td colspan =\"3\">";
					echo $row['description'] . "</td>";
					echo "</tr><tr><td colspan =\"4\"><hr></td>\n";
				}
			echo "</table>\n";
			}
		echo "</div><div id='profileright'>";
		$TAs = $s->getTACourses($profile);
			if (empty($TAs[0])) {
			}
			else {
				foreach ($TAs as $row) {
					{
            echo "<h3>TA of:</h3>";
            echo "<table>";
						echo "<tr><td><a href=\"./community.php?cid=" . $row['cid'] . "\">" . 
						  $row['department'] . $row['course_number'] . " - " . $row['name'] . "</a></td>";
					echo "<td>" . ucfirst($row['semester']) . " " . $row['year'] . "</td></tr>";
					}
				}
			}
					echo "</table>";


		if ($p_arr['show_following']) {
			echo "<h3>Clubs</h3>";
			$c = new CommunityInfo($conn);
			$clubs = $c->getUserClubs($profile);
			echo "<table>";
			if (empty($clubs[0])) {
				echo "No clubs joined yet!<br>";
			}
			else {
				foreach ($clubs as $row) {
					if($row['approved'] == 1) {
						echo "<tr><td><a href=\"./community.php?cid=" . $row['cid'] . "\">" . $row['name'] . "</a></td></tr>";
					}
				}
			}
			echo "</table>";
			echo "<h3>Groups</h3>";
			$groups = $c->getUserGroups($profile);
			echo "<table>";
			if (empty($groups[0])) {
				echo "No groups joined yet!<br>";
			}
			else {
				foreach ($groups as $row) {
					if($row['approved'] == 1) {
						echo "<tr><td><a href=\"./community.php?cid=" . $row['cid'] . "\">" . $row['name'] . "</a></td></tr>";
					}
				}
			}
			echo "</table>";
			echo "<h3>Courses</h3>";
			$c = new CourseInfo($conn);
      $courses = $c->getUserCourses($profile);
			echo "<table>";
			if (empty($courses[0])) {
				echo "No courses joined yet!<br>";
			}
			else {
				foreach ($courses as $row) {
					if($row['approved'] == 1) {
						echo "<tr><td><a href=\"./community.php?cid=" . $row['id'] . "\">" . 
						  $row['department'] . $row['course_number'] . " - " . $row['name'] . "</a></td>";
					echo "<td>" . ucfirst($row['semester']) . " " . $row['year'] . "</td></tr>";
					}
				}
			}
			echo "</table>";

			
		}

	}
	else if ($faculty = $f->getFaculty($profile)) {
		
	// breaks when made into a function for some reason.
		$i = new IDinfo($conn);
		$id = $i->getID($profile);
		$p_arr = array_merge($faculty,$id);
	// -------------------------------------------------

		echo "<div class='title'><h2>" . ucwords($p_arr['first_name']) . " " . ucwords($p_arr['last_name']) . "</h2></div><div class=\"container\">" . $p_arr['position'] . "<br />"
		. "<i>joined " .  $p_arr['start_year'] . "</i><br />"
		. "<a href=\"mailto:" . $p_arr['email'] . "\">email " . $p_arr['first_name'] . "</a>";

			echo "<br><br><h3>Courses Taught By Prof " . ucwords($p_arr['last_name']) . "</h3>";
			$c = new CourseInfo($conn);
			$courses = $c->getCoursesByFaculty($profile);
			echo "<table id='faccourse'>";
			if (empty($courses[0])) {
				echo "No courses taught yet!<br>";
			}
			else {
				foreach ($courses as $row) {
					if($row['approved'] == 1) {
						echo "<tr><td><a href=\"./community.php?cid=" . $row['id'] . "\">" . 
						  $row['department'] . $row['course_number'] . " - " . $row['name'] . "</a></td>";
					echo "<td>" . ucfirst($row['semester']) . " " . $row['year'] . "</td></tr>";
					}
				}
			}
			echo "</table><br><br>";
			echo "<br><br><h3>Jobs, Projects & Experiences</h3>";
			$username = $profile;
			$e = new ExperienceInfo($conn);
			$alljobs = $e->getExperiences($username);
			
			// add the table headers
			echo "</tr>";// display data
			echo "</tr>";// display data
			if (empty($alljobs[0])) {
				echo "No jobs, projects, or experiences added yet!<br>";
			}
			else {
				echo "<table><tr>\n";
				foreach ($alljobs as $row) {
					print "<tr><td>";
					echo $row['company'] . "</td><td><i>";
					echo $row['type'] . "</i></td><td>";
					$date = new DateTime($row['start_date']);
					echo date_format($date, "m/j/Y") . "</td>";
					if ($row['end_date'] != "0000-00-00") {
						$edate = new DateTime($row['end_date']);
						echo "<td>" . date_format($edate, "m/j/Y") . "</td>";
					}
					else {
						echo "<td>present</td>";
					}
					echo "</tr><tr><td>";
					echo $row['position'] . "</td><td colspan =\"3\">";
					echo $row['description'] . "</td>";
					echo "</tr><tr><td colspan =\"4\"><hr></td>\n";
				}
			print "</table>\n";
			}
		echo "";

	}

	else {
		echo "<div class='error'>hmm something is wrong... we can't find that person.<br>Maybe try <a href='./userDirectory.php'>searching</a> for them.</div>";
	}
}

?>
</div></div></div>
</body>
</html>
