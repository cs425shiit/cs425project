<html>
<head><title>Directory</title></head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>Directory</h2></div><div class="container">

<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';
  include './inc/Search.php';
$username = $_COOKIE["test"];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else { 
	echo "Find a student or faculty member: ";
?>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><input type="text" placeholder = "enter full or partial name" name="query"><input type="submit" name ="search" value = "search"/></form>
<?php
	if(isset($_POST['search'])) {
		$search = new Search($conn);
		$count = 0;
		$results = $search->getNamesLike($_POST['query'], $count);
		if(!$count) {
			echo "Sorry, we couldn't find them! Try using a different spelling or searching a partial name!";
		}
		else {
			echo "$count results:<br>";
			foreach ($results as $row) {
    			echo "<a href=\"./profile.php?uid=" . $row['id'] . 					"\">" . $row['first_name'] . " " . $row['last_name'];
				echo "</a><br>";
    		} 
		}
	}
}
?></div>
</body>
</html>
