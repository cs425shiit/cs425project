<html>
<?php
include './inc/connect.inc';

echo $_POST['table'];
$sql = "TRUNCATE " . $_POST['table'] . ";";

$result = $conn->prepare($sql);

if(!$result){
    echo "Error preparing to delete: " . $conn->errorInfo();
} else{
    try{
        $result->execute();
        echo "Table successfully cleared.";
    } catch (PDOException $e){
        echo "Error executing delete statement: " . $e;
    }
}

echo "Click return to be redirected to displayTable";
?>
<form action="displayTable.php" method="post">
    <fieldset>
        <input type="hidden" name="table" value="<?php echo $_POST['table'] ?>" />
        <input type="submit" value="Return">
    </fieldset>
</form>
</html>
