<html>
<?php
include './inc/connect.inc';

$sql = file_get_contents('./sql/massInsert.sql');

$success = $conn->exec($sql);

if ($success){
    echo "Data was entered successfully.";
}
else{
    echo "Oops: Something went wrong. " . $conn->errorInfo();
}

?>
<a href="./adminPage.php">Return to AdminPage</a>
</html>
