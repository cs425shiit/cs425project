<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Oxygen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#register").click(function(){
        $("#reg").fadeIn("slow");
		$("#register").fade();

    });
});
</script>
<script>
$(document).ready(function(){
    $("#login").click(function(){
        $("#log").fadeIn("slow");

    });
});
</script>
<style>
html { 
  background: url(inc/mtccbg.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
a {
	text-decoration: none;
	color: white;
	}
a:hover {
	color: orange;
}
body {
	font-family: 'Oxygen', serif;
}
form {
	display: block;
	margin: 0;
	padding: 0;
}
form input {
    width: 80%;
	display: block;
    padding: 20px;
 	margin: 0 auto 5% auto;
    box-sizing: border-box;
	background-color: rgba(255, 255, 255, 0.7);
    color: rgba(0, 0, 0, 0.7);
	font-size: 16px;
	text-align: center;
    border: none;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}
form input:focus {
 	background-color: rgba(255, 255, 255, 0.8);
}
button {
	background-color: rgba(0, 0, 0, 0.7) ;
    	border: none;
    	color: rgba(255, 255, 255, 0.7);
	width: 80%;
	display: block;
	margin: 0 auto 5% auto;
    	text-align: center;
    	text-decoration: none;
    	font-size: 25px;
   	padding: 20px;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
	cursor:pointer;
}

.brap button {
	background-color: rgba(255, 255, 255, 0.6) ;
    	border: none;
   	color: rgba(0, 0, 0, 0.9);
	width: 80%;
	margin: 0 auto 5% auto;
    	text-align: center;
    	text-decoration: none;
    	display: block;
    	font-size: 25px;
   	padding: 20px 20px;
	-webkit-border-radius: 8px;
	-moz-border-radius: 8px;
	border-radius: 8px;
}
.brap button:hover {
		background-color: rgba(255, 255, 255, 0.8) ;
}
#reg, #log {
	display: none;
}

#focus {
	font-size: 1.2em;
}
#header {
	background-color: rgba(0, 0, 0, 0.5);
    	position: fixed;
	left: 0;
    	top: 0;
    	width: 100%;
	color: white;
	padding: 10px;
}

#container {
	display: block;
    	width:600px;
    	margin: 0 auto 20% auto;
}


#left {

    width:300px;
	float: left;

}

#right {

    width:300px;
	float: left;
}

#footer {
	background-color: rgba(0, 0, 0, 0.5);
    position: fixed;
	left: 0;
    bottom: 0;
    width: 100%;
	color: white;
	margin-top: 10px;
	padding: 10px;
}
</style>
<title>hawkBoard</title>
<head>
<body>
<?php include_once("./inc/analyticstracking.php")?>
<div id="header">
	<h2>CS425 Project | <a href="./S16project.pdf">the project specs</a> | <a href="./README.html">the README</a></h2>
        <div id="focus">Test it out with usernames <b>smith2000</b> or <b>morton2016</b>, password: <b>test</b></div><br>Or go ahead and sign up, sign in, & break stuff. Make up an email address. There's no verification process. This is just a demo.
</div>

<div id="container">
	<div id="left">
        <?php
		include './inc/IDinfo.php';
		include './inc/connect.inc';
		$cookie_value = $_COOKIE["test"];
		if(isset($cookie_value)){
			$i = new IDinfo($conn);
			$id = $i->getID($cookie_value);
			echo "<a href=\"/cs425project/home.php\"><button class='brap'>home</button></a>";
			echo "<a href=\"/cs425project/logout.php\"><button class=\"brap\">logout</button></a>";
		}
		else {
			echo "<button id=\"login\">login</button>
	<div id=\"log\">";
			if(isset($_GET['success']) && $_GET['success'] == 0){
    echo "Login failed, please try again <br/>";
  }
	echo "<form name=\"form1\" method=\"POST\" action=\"verifylogin.php\">"
    . "<input type=\"text\" name=\"username\" placeholder=\"Username\"/>"
    . "<input type=\"password\" name=\"password\" placeholder=\"Password\">"
    . "<button class=\"brap\" type=\"submit\" value=\"login\"/>go</button>"
  	. "</form>";
			echo "</div>";
		}
		?>
    
	</div>
	<div id="right">
	<button id="register">register</button>
	<div id="reg">
   <a href="/cs425project/register.php" class="brap"><button class="brap">student</button></a><a href="/cs425project/facultyregister.php" class="brap"><button class="brap">faculty</button></a>
	</div>
  </div>
        </div>
	
</div>
<div id="footer">
        Created by: <a href="mailto:staritaaj@gmail.com">AJ Starita</a>, Andrew Hessler, & Anthony Fleck
</div>



</body>
</html>
