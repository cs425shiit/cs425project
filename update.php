<html>
<head><title>Update Profile</title></head>
<body>
<?php include_once './inc/nav.html';?>
<div class='title'><h2>Update Profile</h2></div><div class="container"><div class='content'><div class='contentleft'>
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';
$username = $_COOKIE["test"];
$s = new StudentInfo($conn);
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";		
}
else {
	$student = $s->getStudent($username);
if(!$student) {
	$f = new FacultyInfo($conn);
	$faculty = $f->getFaculty($username);
	if($faculty) {

	 ?>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"><fieldset>
		<legend>Profile Info:</legend>
		First name:<br>
		<input type="text" name="first_name" value="<?php echo $faculty['first_name'];?>" required><br>
		Last name:<br>
		<input type="text" name="last_name" value="<?php echo $faculty['last_name'];?>" required><br>
		Year you started at Illinois Tech:<br>
		<input type="number" name="start_year" min="1900" max="2016" value="<?php echo $faculty['start_year'];?>" required><br>
		Current position:<br>
		<input type="text" name="position" required maxlength="50" value="<?php echo $faculty['position'];?>">
		</select><br><br>
	<input type="submit" value="update" name="changeprof"><?php if (isset($_POST['changeprof'])) {
			$success = $f->updateFacultyInfo($_POST,$faculty['id']);
			if ($success) {
				echo "Your change has been submitted.  <i><a href=\"./home.php\">Back to home page</a></i>";
			}
			else 
				echo "i dunno";
		} ?>

	</fieldset></form></div>
	<div class="contentright">
	<form>
	<fieldset> 
		<legend>Account Info:</legend>
		<a href = "./updatepassword.php">Update password</a>
	</fieldset></form>
	<form><fieldset><legend>Course information:</legend>
		<a href="./facultyManageCourses.php">Update courses</a></fieldset></form>
		<form><fieldset><legend>Job/Project information:</legend>
		<a href="./addexperiences.php">Update jobs & projects</a></fieldset>
	</form>

	</div>
	<?php	
		
	
	}}
else {
	 ?>

	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<fieldset>
		<legend>Profile Info:</legend>
		First name:<br>
		<input type="text" name="first_name" value="<?php echo $student['first_name'];?>" required><br>
		Last name:<br>
		<input type="text" name="last_name" value="<?php echo $student['last_name'];?>" required><br>
		Year you started at Illinois Tech:<br>
		<input type="number" name="start_year" min="1900" max="2016" value="<?php echo $student['start_year'];?>" required><br>
		Semester:
		<select name="start_semester">
	<?php
		if ($student['start_semester'] == 'fall') {
			echo  "<option value=\"fall\" selected>Fall</option>"
				. "<option value=\"spring\">Spring</option>"
				. "<option value=\"summer\">Summer</option>"; 
		}
		else if ($student['start_semester'] == 'spring') {
			echo  "<option value=\"fall\">Fall</option>"
				. "<option value=\"spring\" selected>Spring</option>"
				. "<option value=\"summer\">Summer</option>"; 
	   }
		else  {
			echo  "<option value=\"fall\">Fall</option>"
				. "<option value=\"spring\">Spring</option>"
				. "<option value=\"summer\" selected>Summer</option>"; 
		}
	?>
		</select><br>
		GPA:<br>
		<input type="number" name="GPA" min="0" max="4" size="3" step=".01" value="<?php echo $student['GPA'];?>"required<br>
	Degree Status: 
		<select name="degree_status">
	<?php
		if ($student['degree_status'] == 'ongoing') {
			echo  "<option value=\"ongoing\" selected>ongoing</option>"
				. "<option value=\"completed\">completed</option>"
				. "<option value=\"leave\">leave of absence</option>"; 
		}
		else if ($student['degree_status'] == 'completed') {
			echo  "<option value=\"ongoing\">ongoing</option>"
				. "<option value=\"completed\" selected>completed</option>"
				. "<option value=\"leave\">leave of absence</option>"; 
		}
		else  {
			echo  "<option value=\"ongoing\">ongoing</option>"
				. "<option value=\"completed\">completed</option>"
				. "<option value=\"leave\" selected>leave of absence</option>"; 
		}
	?>
		</select><br><br>
		Degree Type: 
		<select name="degree_type">
	<?php
		if ($student['degree_type'] == 'bachelors') {
			echo  "<option value=\"bachelors\" selected>Bachelor's</option>"
				. "<option value=\"masters\">Master's</option>"
				. "<option value=\"phd\">PhD</option>"; 
		}
		else if ($student['degree_type'] == 'masters') {
			echo  "<option value=\"bachelors\">Bachelor's</option>"
				. "<option value=\"masters\" selected>Master's</option>"
				. "<option value=\"phd\">PhD</option>"; 
		}
		else  {
			echo  "<option value=\"bachelors\">Bachelor's</option>"
				. "<option value=\"masters\">Master's</option>"
				. "<option value=\"phd\" selected>PhD</option>"; 
		} 
	?>
		</select><br><br>
	<input type="submit" value="update" name="changeprof"><?php if (isset($_POST['changeprof'])) {
		$success = $s->updateStudentInfo($_POST,$student['id']);
		if ($success) 
				echo "Your change has been submitted. <i><a href=\"./home.php\">Back to home page</a></i>";
			else 
				echo "i dunno";
		} ?> 
	</fieldset></form>
	</div><div class="contentright">
	<form>
	<fieldset> 
		<legend>Account Info:</legend>
		<a href = "./updatepassword.php">Update password</a><br>
		<a href = "./changesettings.php">Change privacy settings</a>
	</fieldset></form>
	<form><fieldset><legend>Job/Internship information:</legend>
		<a href="./addexperiences.php">Update jobs & internships</a></fieldset>
	</form>
	</div>

<?php 
} }
?></div></div>
</body>
</html>
