<html>
<head><title>Community</title></head>
<body>
<?php include_once './inc/nav.html'; ?>
<?php
// if user isn't logged in -> you gotta login, bro
// get the type of the community ID
// regardless of type, give users option to join/login
// if it's a course show: name, department, course #, teacher, semester, year, TA
// 			avgGPA(if any), discussions
// if it's a group or club show: name, club or group, moderators, discussions
// possible implementation: if a commuser is signed in, let them see other users in that community.
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
//  include './inc/SettingsInfo.php';
//  include './inc/StudentInfo.php';
  include './inc/FacultyInfo.php';
  include './inc/CommunityInfo.php';
  include './inc/CourseInfo.php';
  include './inc/MessageBoard.php';
  include_once './inc/IDinfo.php';
$username = $_COOKIE["test"];
$i = new IDinfo($conn);
$visitor = $i->getID($username);
$cid = $_GET['cid'];
$c = new CommunityInfo($conn);
$comm = $c->getCommunityfromID($cid);
$type = $comm['type'];
if(!isset($username)) {
	echo "<div class='error'>Sorry! You must be signed in to do that!</div>";
	include "./inc/loginscript.php";	
}
else {

	if(empty($comm) || !$comm['approved']) {
		echo "<div class='error'>hmm something is wrong... we can't find that page.<br>Maybe try <a href='./communities.php'>searching for it</a>.</div>";
	}
	else {
		if ($type == 'course') {
		echo "<div class='whisper'>Course</div>";
		// if it's a course show: name, department, course #, teacher, semester, year, TA
	// 			avgGPA(if any), discussions
		$k = new CourseInfo($conn);
		$c_arr = $k->getCoursefromID($cid);
		$f = new FacultyInfo($conn);
		$prof = $f->getFaculty($c_arr['teacher']);
		$avgstmt = " - Avg Grade: " . ($c_arr['GPA'] ? number_format($c_arr['GPA'], 2) : " - ");
		echo "<div class='title'><h2>" . $c_arr['department'] . $c_arr['course_number'] . " - ". $c_arr['name'] . "</h2></div><div class='container'><div class='content'><div class='contentleft'>"
			 . "<h3>" . $c_arr['semester'] . " " . $c_arr['year'] . $avgstmt . "</h3>"
			 . "Professor: <a href=\"profile.php?uid=" . $c_arr['teacher'] . "\">" . $prof['first_name'] . " " . $prof['last_name'] . "</a><br />";

		echo "<table><tr><th colspan='3'>Average grades for this course</th></tr>" .
			 "<tr><th> </th><th>Prof " . $prof['last_name'] . "</th><th>all professors</th></tr>" .
			 "<tr><th>High</th><td>" . number_format($c->getFacultyCourseAvgHi($c_arr)['GPA'], 2) . "</td><td>" .
								    number_format($c->getCourseAvgHi($c_arr)['GPA'], 2) . "</td></tr>" . 
			 "<tr><th>Low</th><td>" . number_format($c->getFacultyCourseAvgLo($c_arr)['GPA'], 2) . "</td><td>" .
								  number_format($c->getCourseAvgLo($c_arr)['GPA'], 2) . "</td></tr>" .
			 "<tr><th colspan='2'>Average grade with this professor for all courses:</th><td>" . number_format($c->getFacultyAvgAll($c_arr['teacher'])['GPA'], 2). "</td></tr></table><br><br />";
			echo "TA(s): ";
			$TAs = $c->getTAs($cid);
			if (empty($TAs)) {
				echo "<table>to be determined.</table>";
			}
			else {
				echo "<table id='TAs'>";
				foreach ($TAs as $row) {
					echo "<tr><td><a href=\"profile.php?uid=" . $row['id'] . "\">" . $row['first_name'] . " " . $row['last_name'] .
						 "</a></td></tr>";
				}
				echo "</table>";
			}
		echo "<br />Moderators: <br />";
			$Mods = $c->getMods($cid);
			if (empty($Mods)) {
				echo "<table>to be determined.</table>";
			}
			else {
				echo "<table id='mods'>";
				foreach ($Mods as $row) {
					if($row['approved'] == 1) {
					echo "<tr><td><a href=\"profile.php?uid=" . $row['id'] . "\">" . $row['first_name'] . " " . $row['last_name'] . "</a></td><td><i>" . $row['username'] . "</i></td></tr>";
				}}
				echo "</table>";
			}
		}

		else {
			if ($type == 'interest') {
				echo "<div class='whisper'>Interest Group</div>";
			}
			else {     
				echo "<div class='whisper'>Club</div>";
			}
			echo "<div class='title'><h3>" . $comm['name'] . "</h3></div><div class='container'><div class='content'><div class='contentleft'><h2>Moderated by: </h2>";
			
			$Mods = $c->getMods($cid);
			if (empty($Mods)) {
				echo "<table><tr><td>to be determined.</td></tr></table>";
			}
			else {
				echo "<table id='mods'>";
				foreach ($Mods as $row) {
					echo "<tr><td><a href=\"profile.php?uid=" . $row['id'] . "\">" . $row['first_name'] . " " . $row['last_name'] . "</a></td><td><i>" . $row['username'] . "</i></td></tr>";
				}
				echo "</table>";
			}

		}
		echo "</div><div class='contentright'>";
		$cuser = $_COOKIE[$cid];
		if(isset($_POST['logout'])) {
			setcookie($cid, $cuser, time() - 3600);
			$_COOKIE[$cid] = 0;
			$cuser = 0;
			echo "you have successfully logged out.";
		}
// AREA FOR LOGGED IN PEOPLE
		if($cuser) {
			echo "<div id='login'>You are currently logged in as:<br><b><i>" . $cuser . "</i></b></div>";
			echo "<div class='member'><form method='POST' action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $cid . "'><button type='submit' name='logout'>log out</button></form></div>";
			$mb = new MessageBoard($conn);
			$recent = $mb->recent5($cid);
			echo "</div></div><div class=\"membersonly\"><h3>Recent Comments</h3>" .
				 "<table id=\"top5\"><tr><th>Board</th><th>Comment</th><th>Posted By</th><th>Time</th></tr>";
			foreach ($recent as $row) {
				$d = new DateTime($row['time_posted']);
				$date = $d->format('M jS, Y<\b\\r>g:i a');
				 echo "<tr><td>" . $mb->Linkify($cid, $row['id'],$row['title']) . "</td>" .
					  "<td>" . $row['message'] . "</td>" .
					  "<td>" . $row['username'] . "</td>" .
					  "<td>" . $date . "</td>" .
					  "</tr>";
				}
			echo  "</table>";

			echo "<b><a href='./discussions.php?commid=$cid'>&#x25b6; See All Discussions</a></b></div>";

			# Mod Approve Users
			if ($c->isMod($cuser, $cid)){
				echo "<a href='./approveUsers.php?cid=$cid'>Approve Users</a>";
			}

		}


		else if(isset($_POST['submit'])) {
			$success = $c->verifylogin($_POST);
			if ($success) {
				if(!$success[approved]) {
					echo "Cannot login at this time. Still awaiting approval from moderators.";
				}
				else {
					$cookie_value = $success['username'];
     				setcookie($cid, $cookie_value, time() + (86400 * 30));
					echo "<div id='login'>You are currently logged in as:<br><b><i>" . $cookie_value . "</i></b></div>";
					echo "<div class='member'><form method='POST' action=''><button type='submit'>Click to unlock member area.</button></form>";
			echo "<form method='POST' action='" . htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $cid . "'><button type='submit' name='logout'>log out</button></form></div>";
		}
				
			}
			else {
					echo "Wrong username / password.";	?>
					<form id='commlog' method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . "?cid=" . $cid ;?>"><fieldset><legend>log in</legend>
			   		<input type="text" name="username" placeholder="Username"/><br>
					<input type="password" name="pass" placeholder="Password"><br>
					<input type="hidden" name="cid" value="<?php echo $cid; ?>">
					<button type="submit" name="submit" value="login">log in</button>
					</fieldset></form>
<?php
		
					echo "<h3><a href=\"joincommunity.php?cid=" . $cid . "\">or register to join!</a></h3>";
				}
			}
		else {
?>
		<form id='commlog' method="POST" action=""><fieldset><legend>log in</legend>
   		<input type="text" name="username" placeholder="Username"/><br>
    	<input type="password" name="pass" placeholder="Password"><br>
		<input type="hidden" name="cid" value="<?php echo $cid; ?>">
    	<button type="submit" name="submit" value="login"/>log in</button>
		</fieldset></form>
<?php
		
		echo "<h3><a href=\"joincommunity.php?cid=" . $cid . "\">or register to join!</a></h3>";
		}

	}
}

?>
</div>
</body>
</html>
