<html>
<head><title>Change Privacy Settings</title></head>
<body>
<?php include_once './inc/nav.html';?>
<div class="title"><h2>Change Privacy Settings</h2></div><div class="container">
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
  include './inc/connect.inc';
  include './inc/StudentInfo.php';
  include './inc/SettingsInfo.php';

$username = $_COOKIE["test"];
$s = new StudentInfo($conn);
$student = $s->getStudent($username);
if(!$student) {
	echo "You must be logged in as a student to change these settings.";
	include './inc/loginscript.php';
}
else {
	$set = new SettingsInfo($conn);
	if (isset($_POST['changeset'])) {
		$success = $set->updateSettings($_POST,$student['id']);
		if ($success) {
			echo "Your change has been submitted. <br> <a href=\"" . htmlspecialchars($_SERVER["PHP_SELF"]) . "\">Back to privacy settings</a>";
			echo "<br><a href = \"./update.php\">Back to main settings page</a>";
		}
		else
			echo "i dunno";
	}
	else {

$settings = $set->getSettings($username);
?>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
	<fieldset>
		<legend>Your privacy settings:</legend><br>
		Year & Semester You Started:<br>
		<label>Public
		<?php
		if ($settings['show_start_semester_and_year'] == 1) {
			echo  "<input type=\"radio\" name=\"startbool\" value=\"1\" checked />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"startbool\" value=\"0\" />";
		} else {
			echo  "<input type=\"radio\" name=\"startbool\" value=\"1\" />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"startbool\" value=\"0\" checked />";
		}

		?>
		</label><br><br>
		Degree Status:<br>
		<label>Public
		<?php
		if ($settings['show_degree_status'] == 1) {
			echo  "<input type=\"radio\" name=\"degstatbool\" value=\"1\" checked />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"degstatbool\" value=\"0\" />";
		} else {
			echo  "<input type=\"radio\" name=\"degstatbool\" value=\"1\" />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"degstatbool\" value=\"0\" checked />";
		}

		?>
		</label><br><br>
		Degree Type:<br>
		<label>Public
		<?php if ($settings['show_degree_type'] == 1) {
			echo  "<input type=\"radio\" name=\"degtypebool\" value=\"1\" checked />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"degtypebool\" value=\"0\" />";
		} else {
			echo  "<input type=\"radio\" name=\"degtypebool\" value=\"1\" />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"degtypebool\" value=\"0\" checked />";
		}

		?>
		</label><br><br>
		GPA:<br>
		<label>Public
		<?php if ($settings['show_GPA'] == 1) {
			echo  "<input type=\"radio\" name=\"gpabool\" value=\"1\" checked />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"gpabool\" value=\"0\" />";
		} else {
			echo  "<input type=\"radio\" name=\"gpabool\" value=\"1\" />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"gpabool\" value=\"0\" checked />";
		}

		?>
		</label><br><br>
		Groups & Clubs You're Following:<br>
		<label>Public
		<?php if ($settings['show_following'] == 1) {
			echo  "<input type=\"radio\" name=\"followbool\" value=\"1\" checked />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"followbool\" value=\"0\" />";
		} else {
			echo  "<input type=\"radio\" name=\"followbool\" value=\"1\" />"
			. "</label>"
			. "<label>Private"
		   	. "<input type=\"radio\" name=\"followbool\" value=\"0\" checked />";
		}

		?>
		</label>
	<br />
		<br><br>
	<input type="submit" value="submit changes" name="changeset">

	</fieldset>
	</form>
<?php
} }
?>
</div>
</body>
</html>
