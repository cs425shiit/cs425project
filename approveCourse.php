<?php
include_once './inc/connect.inc';
include_once './inc/CourseInfo.php';
echo "<h3>COURSES TO BE APPROVED</h3>";
$c = new CourseInfo($conn);
$unapproved = $c->getUnapprovedCourses();
if (!$unapproved) {
	echo "No courses awaiting approval at this time.";
}
else {
	echo "<table border=\"0|0\"><tr>\n";
		// add the table headers
	echo "<th>Dept</th><th>Course No.</th><th>Semester</th><th>Year</th><th>&#x2714;</th><th>&#x2718;</th></tr></tr><tr><td colspan =\"6\"><hr></td></tr>";// display data

	foreach($unapproved as $row){
		print "<tr><td>";
		echo $row['department'] . "</td><td>";
		echo $row['course_number'] . "</td><td>";
		echo $row['semester'] . "</td><td>";
		echo $row['year'] . "</td>";
	  echo "<td><a href =\"./adminApproveCourse.php?cid=" . $row['id'] . "\">approve</a></td>";
		echo "<td><a href =\"./adminDeleteCourse.php?cid=" . $row['id'] . "\">delete</a></td>";
	  echo "</tr><tr><td colspan =\"6\"><hr></td>\n";

		}
	print "</table>";
}
?>
